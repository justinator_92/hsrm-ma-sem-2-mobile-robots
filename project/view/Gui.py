from Tkinter import *
from Canvas import *
from pathfinder import Space
from geometry import Point

import tkFileDialog
import sys

WIDTH = 1200  # width of canvas
HEIGHT = 650  # height of canvas

TITLE = "D* Simulation by Justin Albert"


class Gui:

    def __init__(self):
        # create main window
        global root
        root = Tk()
        root.title(TITLE)
        root.resizable(False, False)

        self.show_grid = BooleanVar()
        self.show_grid.set(True)

        # create and position canvas and buttons
        cFr = Frame(root, width=WIDTH, height=HEIGHT, relief="sunken", bd=1)
        cFr.pack(side="top")

        # define options for opening or saving a file
        self.file_opt = options = {}

        can = Canvas(cFr, width=WIDTH, height=HEIGHT, bg="#ffffff")
        can.pack()

        img = PhotoImage(width=WIDTH, height=HEIGHT)
        can.create_image((WIDTH/2, HEIGHT/2), image=img, state="normal")

        cFr = Frame(root)
        cFr.pack(side="left")
        eFr = Frame(root)
        eFr.pack(side="right")

        self.space = Space(can, img, WIDTH, HEIGHT, root)  # init controller object...
        self.create_menu()

        can.bind("<ButtonPress-1>", self.space.on_button_press)
        can.bind("<ButtonRelease-1>", self.space.on_button_release)
        root.mainloop()

    def create_menu(self):
        """
        Create Menubar
        :return:
        """

        menubar = Menu(root, tearoff=0)
        menubar.add_command(label="Undo", command=self.click)
        menubar.add_command(label="Redo", command=self.click)

        menubar = Menu(root)

        # create a pulldown menu, and add it to the menu bar
        filemenu = Menu(menubar, tearoff=0)
        filemenu.add_command(label="Open Configuration", command=self.askopenfile)
        filemenu.add_command(label="Save Configuration", command=self.asksaveasfilename)
        filemenu.add_separator()
        filemenu.add_command(label="Exit", command=root.quit)
        menubar.add_cascade(label="File", menu=filemenu)

        # create more pulldown menus
        editmenu = Menu(menubar, tearoff=0)
        editmenu.add_command(label="New Fixpoints", command=self.space.robot.make_new_grid)
        editmenu.add_checkbutton(label="Draw Spline", onvalue=True, offvalue=False, command=self.reload_fixpoints,
                                 variable=self.show_grid)

        editmenu.add_command(label ="Set Robot", command=self.set_robot)

        menubar.add_cascade(label="Options", menu=editmenu)

        # display the menu
        root.config(menu=menubar)

    def set_robot(self):
        self.space.set_robot = True

    def askopenfile(self):
        """Returns an opened file in read mode."""
        # get filename
        filename = tkFileDialog.askopenfilename(**self.file_opt)
        print filename
        if filename is None or filename == "":
            self.space.load_configuration(filename)

    def reload_fixpoints(self):
        """ calculate new fixpoints """
        val = self.show_grid.get()
        print val
        self.space.robot.DrawSpline = val

    def asksaveasfilename(self):
        """
        Returns an opened file in write mode.
        This time the dialog just returns a filename and the file is opened by your own code.
        """
        filename = tkFileDialog.asksaveasfilename(**self.file_opt)
        self.space.save_configuration(filename)

    def click(self):
        pass
        #self.space.robot.move_robot(Point(300, 500))

    def quit(self, root=None):
        """ quit programm """
        if not root:
            sys.exit(0)
        root._root().quit()
        root._root().destroy()


if __name__ == "__main__":
    a = Gui()
