from objects.Primitive import Primitive
from geometry import Point
from utils import GeometryUtils as g

import os

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.sys.path.insert(0, parentdir)


class Triangle(Primitive):

    def __init__(self, a, b, c, color):
        """
        constructor for Triangle
        :param a:
        :param b:
        :param c:
        """

        Primitive.__init__(self, color)

        self.a = a
        self.b = b
        self.c = c

        self.center = g.barycentric_combination([self.a, self.b, self.c])

    def draw(self, canvas):
        """
        draw shape on canvas
        :param canvas:
        :return:
        """

        points = [self.a.x, self.a.y, self.b.x, self.b.y, self.c.x, self.c.y]

        self.draw_minkowski(canvas)
        return canvas.create_polygon(points, fill=self.color)

    def calculate_points(self):
        """
        Calculate points
        :return:
        """

        polygon = [self.a, self.b, self.c, self.a]

        for x in range(self.p0.x, self.p2.x + 1):
            for y in range(self.p0.y, self.p2.y + 1):

                p = Point(x, y)
                if self.intersect(p, polygon):
                    self.points.append(p)


    def rotate(self, angle):
        """
        rotate the rectangle around angle
        :param angle: angle in radians
        """

        self.a = g.rotate_around_point(self.a, self.center, angle)
        self.b = g.rotate_around_point(self.b, self.center, angle)
        self.c = g.rotate_around_point(self.c, self.center, angle)

        return [self.a.x, self.a.y, self.b.x, self.b.y, self.c.x, self.c.y]

    def __repr__(self):
        return "Triangle: [a: %s, b: %s, c: %s, center: %s]" % (self.a, self.b, self.c, self.center)
