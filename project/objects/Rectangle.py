from geometry import Point, Line, Vector
from Primitive import Primitive
from utils import GeometryUtils as g

import os
import math

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.sys.path.insert(0, parentdir)


class Rectangle(Primitive):

    def __init__(self, p1, p2, color):

        """ constructor for Rectangle """
        Primitive.__init__(self, color)
        self.p0 = p1  # lower left
        self.p1 = Point(p1.x, p2.y)  # upper left
        self.p2 = p2  # upper right
        self.p3 = Point(p2.x, p1.y)  # lower right

        size = p2 - p1  # width, height
        center = Point(size.x / 2, size.y / 2)
        self.center = center + Vector(p1.x, p1.y)

        self.calculate_points()  # calculate point

    def rotate(self, angle):
        """
        rotate the rectangle around angle
        :param angle: angle in radians
        """

        self.p0 = g.rotate_around_point(self.p0, self.center, angle)
        self.p1 = g.rotate_around_point(self.p1, self.center, angle)
        self.p2 = g.rotate_around_point(self.p2, self.center, angle)
        self.p3 = g.rotate_around_point(self.p3, self.center, angle)

        return [self.p0.x, self.p0.y, self.p1.x, self.p1.y,
                self.p2.x, self.p2.y, self.p3.x, self.p3.y]

    def draw(self, canvas):
        """
        draw shape on canvas
        :param canvas:
        :return:
        """

        points = [self.p0.x, self.p0.y, self.p1.x, self.p1.y, self.p2.x, self.p2.y, self.p3.x, self.p3.y]

        self.draw_minkowski(canvas)
        return canvas.create_polygon(points, fill=self.color)

    def calculate_points(self):

        for y in range(self.p0.y, self.p1.y):
            self.points.append(Point(self.p0.x, y))
            self.points.append(Point(self.p3.x, y))

        for x in range(self.p0.x, self.p3.x):
            self.points.append(Point(x, self.p0.y))
            self.points.append(Point(x, self.p1.y))

    def __repr__(self):
        """
        string representation of rectangle object
        :return:
        """
        return "Rect: [p0: %s, p2: %s, Points: %d, mink: %d]" % (self.p0, self.p2, len(self.points), len(self.min))

if __name__ == '__main__':

    # create robot
    robot = Rectangle(Point(10, 10), Point(10, 10), "#ffffff")

    # create rectangle of interest...
    rect = Rectangle(Point(100, 100), Point(150, 150), "#ffffff")
    rect.minkowski(robot)

    # first case, no intersection
    print "test first case!!! \n"
    line1 = Line(Point(200, 200), Point(200, 300))
    print rect.calculate_intersection(line1)

    # second case, line starts in object
    print "test second case!!!\n"
    line2 = Line(Point(125, 125), Point(400, 200))
    print rect.calculate_intersection(line2)

    # third case, regular intersection
    print "test third case!!!\n"
    line3 = Line(Point(125, 50), Point(175, 200))
    print rect.calculate_intersection(line3)

