from geometry import Point, Line
from utils import GeometryUtils as g


import math


class Primitive(object):
    """
    Base class for Primitive objects...
    """

    def __init__(self, color):
        """ constructor for Primitive geometric object """

        self.points = []
        self.min = []
        self.boundingBox = None
        self.color = color

    def minkowski(self, obj):
        """ calculates Minkowski sum """

        points = set()

        for pointA in self.points:
            for pointB in obj.points:
                vector = pointB - obj.center
                p = pointA + vector
                if p.x >= 0 and p.y >= 0:
                    points.add(p)

        self.min = g.convex_hull(list(points))

    def point_intersection(self, point):
        return g.point_in_polygon(self.min, point)

    def calculate_intersection(self, other):

        # check if start- and endpoint of line is in polygon...
        if g.point_in_polygon(self.min, other.p1) or g.point_in_polygon(self.min, other.p2):
            print "point is in polygon... TRUE!!!"
            return True

        # make lines from points in minkowski sum:
        for (p1, p2) in zip(self.min, self.min[1:]):
            line = Line(p1, p2)
            p = line.intersect_lines(other)

            if p and g.point_between_points(other.p1, other.p2, p) and g.point_between_points(p1, p2, p):
                return True

        return False

    def draw_minkowski(self, canvas):
        """ draw minkowski sum to canvas """
        if len(self.min) > 0:
            c_id = canvas.create_polygon([[p.x, p.y] for p in self.min], fill="#80C2FF", outline=None)
            canvas.tag_lower(c_id)
            return c_id
