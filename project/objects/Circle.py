from geometry import Point
from objects.Primitive import Primitive
from geometry import Line

import os
import math

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.sys.path.insert(0, parentdir)


class Circle(Primitive):

    def __init__(self, center, radius, color):
        """ constructor for circle """
        Primitive.__init__(self, color)

        self.center = center
        self.radius = radius

        self.points = []  # list that includes all points...
        self.calculate_points()  # calculate own points...

    def intersect(self, p):
        """ test if point intersects with circle """

        if (self.center - p).length() < self.radius:
            self.points.append(p)
            return True

        return False

    def calculate_points(self):
        """
        calulates Points
        :return:
        """

        for t in range(0, 360, 10):
            radians = t * math.pi / 180.
            point = Point(self.radius * math.cos(radians) + self.center.x,
                          self.radius * math.sin(radians) + self.center.y)

            self.points.append(point)

    def draw(self, canvas):
        """
        draw a circle on a canvas
        :param canvas:
        :return:
        """

        self.draw_minkowski(canvas)
        return canvas.create_oval(self.center.x - self.radius, self.center.y - self.radius,
                                  self.center.x + self.radius, self.center.y + self.radius,
                                  fill=self.color, outline=self.color)

    def __repr__(self):
        """
        string representation
        :return:
        """
        return "Circle [Center: %s, Radius: %d, Points: %d]" % (self.center, self.radius, len(self.points))

if __name__ == '__main__':

    robot = Circle(Point(10, 10), 1, "#ffffff")

    # check second case...
    rect = Circle(Point(100, 100), 50, "#ffffff")
    rect.minkowski(robot)
    line = Line(Point(110, 110), Point(400, 200))

    rect.calculate_intersection(line)
