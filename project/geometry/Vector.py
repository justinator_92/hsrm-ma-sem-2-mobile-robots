import math


class Vector(object):

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def normalized(self):
        l = self.length()
        if l != 0:
            self.x /= l
            self.y /= l
        return Vector(self.x, self.y)

    def normalize(self):
        l = self.length()
        if l != 0:
            return Vector(self.x / l, self.y / l)
        return Vector(self.x, self.y)

    def length(self):
        return math.sqrt(self.x ** 2 + self.y ** 2)

    def __repr__(self):
        return "Vector (%.9f, %.9f)" % (self.x, self.y)

    def __add__(self, v):
        return Vector(self.x + v.x, self.y + v.y)

    def __sub__(self, v):
        return Vector(self.x - v.x, self.y - v.y)

    def __rsub__(self, v):
        return self.__sub__(v)

    def __div__(self, v):
        assert type(v) == float, "can't divide vector with" + v
        return Vector(self.x / v, self.y / v)

    def __mul__(self, v):
        if type(v) == type(self):
            return Vector(self.x * v.a, self.y * v.b)
        if type(v) in [float, int]:  # skalares multiplizieren
            return Vector(self.x * v, self.y * v)
        return None

    def __rmul__(self, v):
        return self.__mul__(v)

    def dot(self, v):
        return self.x * v.x + self.y * v.y

    def scale(self, number):
        return Vector(self.x * number, self.y * number)

    def normal(self):
        return Vector(-self.y, self.x)

    def cross(self, other):
        return self.x * other.y - self.y * other.x
