from geometry import Vector


class Point(object):
    """
    Punktklasse mit allen benoetigten Operationen im R3'''
    """

    def __init__(self, x, y):
        """
        constructor initializes a new instance of Point object
        with x and y coordinates in 2 Dimensional room
        """
        self.x = x
        self.y = y

    def __repr__(self):
        """
        string representation of Point
        :return: string representation
        """
        return "Point (%f, %f)\n" % (self.x, self.y)

    def __sub__(self, p1):
        """ subtrahiert komponentenweise """
        if type(p1) == Vector:
            return Point(self.x - p1.x, self.y - p1.y)
#         if type(p1) == type(self):
        return Vector(self.x - p1.x, self.y - p1.y)
  #      return None

    def __add__(self, v):
        ''' addiert einen Vektor mit einem Punkt komponentenweise '''
        assert type(v) == Vector, "Addition muss Punkt + Vektor sein"
        return Point(self.x + v.x, self.y + v.y)

    def __eq__(self, other):
        return self.__key() == other.__key()

    def __ne__(self, other):
        return not self == other

    def __key(self):
        return self.x, self.y

    def __hash__(self):
        return hash(self.__key())

    def __repr__(self):
        return "Point: [x=%f, y=%f]" % (self.x, self.y)

    def __gt__(self, other):
        assert False
        if self.x > other.x:
            return True
        if self.x == other.x:
            return self.y >= other.y

        return False

    def __lt__(self, other):

        if self.x < other.x:
            return True

        if self.x == other.x:
            return self.y <= other.y

        return False


if __name__ == '__main__':

    p1 = Point(67, 10)
    p2 = Point(67, 20)
    p3 = Point(67, 2)
    p4 = Point(80, 0)
    p5 = Point(90, 0)

