import Queue as Q
import Node


class State(object):

    def __init__(self, keys, node):

        self.keys = keys
        self.node = node

    def __cmp__(self, other):
        return cmp(self.keys, other.keys)

    def __repr__(self):
        return "State [primary: %f, secondary: %f, object: %s" % (self.keys[0], self.keys[1], self.node)


class Pathfinder(object):

    def __init__(self, nodes):
        self.open_list = Q.PriorityQueue()  # a priority queue
        self.nodes = nodes

        self.start = None
        self.goal = None

    def reset_nodes(self, nodes):
        self.start = None
        self.goal = None

        self.nodes = nodes
        self.open_list = Q.PriorityQueue()

    def update_state_in_open_list(self, u, new_key):

        state = self.object_in_open_list(u)

        if state:
            self.open_list.queue.remove(state)
            self.open_list.put(State(new_key, u))

    def remove_state_from_open_list(self, node):

        state = self.object_in_open_list(node)

        if state:
            self.open_list.queue.remove(state)

    def object_in_open_list(self, node):
        l = filter(lambda x: x.node == node, self.open_list.queue)  # iterate over all elements in priority queue
        if len(l) >= 1:
            return l[0]
        return False

    def procedure_initialize(self):
        self.open_list = Q.PriorityQueue()

        for node in self.nodes:
            node.g = float('inf')
            node.rhs = float('inf')

        self.goal.rhs = 0

        self.open_list.put(State(self.calculate_key(self.goal), self.goal))

    def min_neighbours(self, u):

        rhs_values = []

        for neighbour, cost in u.neighbours.items():
            rhs_values.append(neighbour.g + cost)

        return min(rhs_values)

    def calculate_key(self, s):
        return min(s.g, s.rhs) + (self.start - s).length(), min(s.g, s.rhs)

    def update_vertex(self, u):

        if u != self.goal:
            u.rhs = self.min_neighbours(u)

        if self.object_in_open_list(u):
            self.remove_state_from_open_list(u)

        if u.g != u.rhs:
            self.open_list.put(State(self.calculate_key(u), u))

    def compute_shortest_path(self):

        while not self.open_list.empty():

            u_state = self.open_list.get()

            if u_state.keys < self.calculate_key(self.start) or self.start.rhs != self.start.g:

                u = u_state.node

                if u.g > u.rhs:
                    u.g = u.rhs

                    for s in u.neighbours:
                        self.update_vertex(s)

                else:
                    u.g = float('inf')
                    for s in u.neighbours.keys() + [u]:
                        self.update_vertex(s)

    def construct_path(self, node):

        path = [node]

        while node != self.goal:
            # find neighbour that minimizes (s.g + c(s',s))
            node = min(node.neighbours.items(), key=lambda n: n[0].g + n[1])[0]
            path.append(node)

        return path

    def dstar_search(self, start, goal):

        self.start = start
        self.goal = goal

        self.procedure_initialize()
        print "move to goal!", self.goal
