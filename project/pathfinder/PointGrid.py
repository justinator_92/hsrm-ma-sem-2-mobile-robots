from geometry import Point, Line
from scipy.spatial import Delaunay
from pathfinder import Node
from utils import Serializer

import random
import numpy as np
import time

SIZE = 3
DISTANCE = 65
RETRY_MAX = 150
NUM_POINTS = 100
WAIT_TIME = 0.05


class PointGrid(object):

    def __init__(self, canvas, root, width, height, robot, callback, objects):

        self.robot = robot

        self.canvas = canvas  # canvas to draw objects
        self.root = root
        self.width = width  # width of canvas
        self.height = height  # height of canvas

        self.grid_points = set()
        self.lines = set()  # list that contains all object id's for drawing on a canvas

        self.objects = objects  # list that contains all obstacles in world
        self.points = []

        self.number_of_points = NUM_POINTS

        self.line = []  # list that contains all lines

        self.callback = callback
        self.serializer = Serializer()

    def generate_points(self):

        points = []
        retries = 0

        while len(points) < self.number_of_points and retries < RETRY_MAX:

            valid = True
            node = Node(random.randint(5, self.width), random.randint(5, self.height))
            #print node

            for fix_point in points:
                if (fix_point - node).length() < DISTANCE:  # optimize variance
                    valid = False
                    break

            if not valid:
                retries += 1
                #print "retries: ", retries
                continue

            for obj in self.objects:
                if obj.point_intersection(node):
                    valid = False
                    break

            if valid:
                #print "appended"
                points.append(node)
                retries = 0  # reset retries after successful placement..

        return points

    def draw_node(self, node, wait=True):

        object_id = self.canvas.create_oval(node.x - SIZE, node.y - SIZE, node.x + SIZE, node.y + SIZE,
                                            tags="event", fill="#b1b1b1", outline="#b1b1b1")

        self.canvas.tag_bind(object_id, '<ButtonPress-3>', lambda event, arg=node: self.callback(event, arg))
        # self.canvas.tag_lower(object_id)

        self.grid_points.add(object_id)

        if wait:
            pass
            #self.root.update()
            #time.sleep(WAIT_TIME)

    def delete_points(self):

        for line in self.lines:
            self.canvas.delete(line)

        for fix in self.grid_points:
            self.canvas.tag_unbind(fix, "<ButtonPress-3>")
            self.canvas.delete(fix)

        self.lines.clear()
        self.grid_points.clear()

    def calculate_triangulation(self, file_name="points.conf"):

        wait = False

        self.delete_points()
        self.points = self.serializer.get_list_objects(file_name)

        if len(self.points) <= 0:
            self.points = self.generate_points()  # generate points on grid
            self.serializer.serialize(self.points, "points.conf")
            wait = True

        for node in self.points:
            self.draw_node(node, wait)

        # TODO: replace with own triangulation...
        pp = np.array([[node.x, node.y] for node in self.points])
        tri = Delaunay(pp)

        for e in tri.simplices:
            node1 = self.points[e[0]]
            node2 = self.points[e[1]]
            node3 = self.points[e[2]]

            lines = {Line(node1, node2): (node1, node2),
                     Line(node2, node3): (node2, node3),
                     Line(node3, node1): (node3, node1)}

            for line, nodes in lines.iteritems():
                valid = True

                for obj in self.objects:
                    #print obj, "has intersection", line.p1, line.p2
                    if obj.calculate_intersection(line):

                        valid = False
                        break

                if valid:  # connection between two points is valid.

                    distance = (nodes[0] - nodes[1]).length()  # calculate vector between points..

                    nodes[0].add_neighbour_costs(nodes[1], distance)
                    nodes[1].add_neighbour_costs(nodes[0], distance)

                    self.lines.add(self.canvas.create_line(line.p1.x, line.p1.y, line.p2.x, line.p2.y, fill="#cccccc"))

            for line in self.lines:
                self.canvas.tag_lower(line)

        #print "point", self.points

    def intersect_obstacle(self, obj):

        for line in self.lines:
            if obj.calculate_intersection(line):
                print "yes"

