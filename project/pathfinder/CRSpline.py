from geometry import Point
from utils import GeometryUtils as g

import numpy as np

RESOLUTION = 10.
SPEED = 2.  # 5 pixel per step


def create_middle(a=1):
    """ create start matrix """
    return np.matrix([[0, 2, 0, 0],
                      [-a, 0, a, 0],
                      [2 * a, -6 + a, 6 - 2 * a, -a],
                      [-a, 4 - a, -4 + a, a]])


def create_start():
    """ create matrix for middle points """
    return np.matrix([[0, 2, 0, 0],
                      [2, 0, 0, 0],
                      [-4, -5, 6, -1],
                      [2, 3, -4, 1]])


def create_end():
    """ create matrix for last point """
    return np.matrix([[0, 2, 0, 0],
                      [-1, 0, 1, 0],
                      [2, -6, 4, -2],
                      [-1, 4, -3, 2]])


class CRSpline(object):

    def __init__(self, start_derivative, end_derivative):
        """ constructor for catmull rom splines curve """
        self.start = create_start()
        self.middle = create_middle()
        self.end = create_end()

        self.start_derivative = start_derivative
        self.end_derivative = end_derivative

    def calculate_first_segment(self, p0, p1, p2, resolution=RESOLUTION):

        point_list = []
        a = 0.5

        for e in range(1, int(resolution) + 1):

            t = e / resolution
            b = np.array([[1, t, t*t, t*t*t]])
            c = self.start
            d = np.array([[1, self.start_derivative], p0, p1, p2])
            p = a * b.dot(c.dot(d))

            point_list.append(p)

        return [Point(point.item((0, 0)), point.item((0, 1))) for point in point_list]

    def calculate_middle_segment(self, p0, p1, p2, p3, resolution=RESOLUTION):

        point_list = []
        a = 0.5

        for e in range(1, int(resolution) + 1):

            t = e / resolution
            b = np.array([[1, t, t*t, t*t*t]])
            c = self.middle
            d = np.array([p0, p1, p2, p3])
            p = a * b.dot(c.dot(d))

            point_list.append(p)

        return [Point(point.item((0, 0)), point.item((0, 1))) for point in point_list]

    def calculate_end_segment(self, p0, p1, p2, resolution=RESOLUTION):
        """ calculate end segment of spline """

        point_list = []

        for e in range(1, int(resolution) + 1):

            t = e / resolution
            b = np.array([[1, t, t*t, t*t*t]])

            d = np.array([p0, p1, p2, [1, self.end_derivative]])
            a = create_end().dot(d)
            temp = b.dot(a)

            p = 0.5 * temp
            point_list.append(p)

        return [Point(point.item((0, 0)), point.item((0, 1))) for point in point_list]

    def create_spline(self, node_list):
        """ create spline curve """

        point_list = [[node.x, node.y] for node in node_list]
        dic = {}

        print point_list
        print node_list

        for i in range(0, len(point_list)-1):

            if i == 0:  # start point

                temp_points = self.calculate_first_segment(point_list[i], point_list[i+1], point_list[i+2])
                dist = g.get_distance_between_points(temp_points)
                res = dist / SPEED
                dic[node_list[i]] = self.calculate_first_segment(point_list[i], point_list[i+1], point_list[i+2], res)
            elif i == len(point_list)-2:  # end points
                temp_points = self.calculate_end_segment(point_list[i - 1], point_list[i], point_list[i + 1])
                dist = g.get_distance_between_points(temp_points)
                res = dist / SPEED
                dic[node_list[i]] = self.calculate_end_segment(point_list[i-1], point_list[i], point_list[i+1], res)
            else:  # regular middle point
                p0, p1, p2, p3 = point_list[i-1], point_list[i], point_list[i+1], point_list[i+2]

                temp_points = self.calculate_middle_segment(p0, p1, p2, p3)
                dist = g.get_distance_between_points(temp_points)
                res = dist / SPEED
                dic[node_list[i]] = self.calculate_middle_segment(p0, p1, p2, p3, res)

        return dic


if __name__ == '__main__':

    points = [Point(20, 5),
              Point(22, 12),
              Point(34, 17),
              Point(34, 24),
              Point(22, 36),
              Point(22, 25)]

    #points = [np.array(p) for p in points]

    spline = CRSpline(0, 0)
    print spline.create_spline(points)
