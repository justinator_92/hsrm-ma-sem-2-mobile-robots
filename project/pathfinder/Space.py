from geometry import Point
from robot.Robot import Robot
from utils import Serializer
from pathfinder import Obstacle
from objects import Rectangle, Circle, Triangle


BLUE = "#0080FF"  # light blue
GREEN = "#FF0000"  # green
LIGHT_BLUE = "#80C2FF"  # even lighter blue
BLACK = "#000000"  # black
RED = "#FF0000"  # red

SIZE = 10
FRAME = 4  # size of frame


class Space:

    def __init__(self, canvas, img, width, height, root):

        # create obstacles...
        o1 = Circle(Point(600, 500), 100, BLUE)
        o2 = Rectangle(Point(150, 100), Point(300, 280), BLUE)
        o3 = Rectangle(Point(150, 370), Point(300, 550), BLUE)
        o4 = Rectangle(Point(600, 100), Point(1100, 200), BLUE)

        o5 = Rectangle(Point(900, 500), Point(1100, 400), BLUE)

        # define borders...
        b1 = Rectangle(Point(0, 0), Point(width, FRAME), BLUE)
        b2 = Rectangle(Point(0, 0), Point(FRAME, height), BLUE)
        b3 = Rectangle(Point(0, height - FRAME), Point(width, height), BLUE)
        b4 = Rectangle(Point(width - FRAME, 0), Point(width, height), BLUE)

        objects = [o1, o2, o3, o4, o5, b1, b2, b3, b4]

        self.robot = Robot(Point(800, 300), BLACK, canvas, root, width, height, objects)

        self.canvas = canvas  # the canvas to draw all objects
        self.img = img  # image to set pixel manually on GUI
        self.dynamic_obstacles_ids = []

        self.x = None
        self.y = None

        self.set_robot = False

    def on_button_press(self, event):
        self.x = event.x
        self.y = event.y

    def on_button_release(self, event):
        p1 = Point(self.x, self.y)
        p2 = Point(event.x, event.y)

        if self.set_robot:
            self.robot.translate_robot(p1 - self.robot.center)
            self.set_robot = False
            return

        obstacle = Obstacle.Obstacle(p1, p2, "#444444")
        self.robot.sensor.add_obstacle(obstacle)
        self.dynamic_obstacles_ids.append(obstacle.draw(self.canvas))

    def delete_obstacles(self):

        for obj in self.dynamic_obstacles_ids:
            self.canvas.delete(obj)

        self.robot.sensor.clear_obstacles()
        self.dynamic_obstacles_ids = []

    def load_configuration(self, filename):
        obj = self.serializer.get_list_objects(filename)
        #if obj:
         #   self.objects = obj
            # self.draw_objects(self.objects)

    def save_configuration(self, filename):
        self.serializer.serialize(self.objects, filename)

if __name__ == '__main__':
    serializer = Serializer()
    objs = serializer.get_list_objects("configuration.conf")
    #print objs
    #points = serializer.get_list_objects("points.conf")

    # p1 = Point(526., 580.)
    # p2 = Point(800., 389.)
    #
    # o = objs[1]
    # print o
    # print "has intersection", o.calculate_intersection(Line(p1, p2))
