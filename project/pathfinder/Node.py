from geometry import Point
from Queue import PriorityQueue


class Node(Point):
    def __init__(self, x, y):
        Point.__init__(self, x, y)

        self.g = float('inf')  # cost
        self.rhs = float('inf')  # one step ahead

        self.neighbours = {}

    def add_neighbour_costs(self, neighbour, cost):
        self.neighbours[neighbour] = cost

    def reset(self):
        self.g = float('inf')
        self.rhs = float('inf')

        self.neighbours = {}

    def __key(self):
        return self.x, self.y

    def __repr__(self):
        return "Node [location: (%f, %f), g: %f, rhs: %f, suc: %d]" % (self.x, self.y, self.g, self.rhs,
                                                                       len(self.neighbours.keys()))


if __name__ == '__main__':
    queue = PriorityQueue()
    queue.put(Node(1, 2))
