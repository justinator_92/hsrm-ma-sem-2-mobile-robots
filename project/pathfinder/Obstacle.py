from geometry import Point, Vector, Line
from utils import GeometryUtils as g

border = 5


class Obstacle(object):

    def __init__(self, p1, p2, color):

        vec = p2 - p1
        distance = vec.length()

        angle = g.get_orientation(Vector(1, 0), vec)

        self.p0 = Point(p1.x - border, p1.y - border)  # lower left
        self.p1 = Point(p1.x - border, p1.y + border)  # upper left
        self.p2 = Point(p1.x + distance + border, p1.y - border)  # upper right
        self.p3 = Point(p1.x + distance + border, p1.y + border)  # lower right

        self.p0 = g.rotate_around_point(self.p0, p1, angle)
        self.p1 = g.rotate_around_point(self.p1, p1, angle)
        self.p2 = g.rotate_around_point(self.p2, p1, angle)
        self.p3 = g.rotate_around_point(self.p3, p1, angle)

        self.color = color

    def draw(self, canvas):
        points = [self.p0.x, self.p0.y, self.p1.x, self.p1.y, self.p3.x, self.p3.y, self.p2.x, self.p2.y]
        return canvas.create_polygon(points, fill=self.color)

    def calculate_intersection(self, other):

        points = [self.p0, self.p1, self.p2, self.p3]

        # make lines from points in minkowski sum:
        for (p1, p2) in zip(points, points[1:]):
            line = Line(p1, p2)
            p = line.intersect_lines(other)
            # print p
            if p and g.point_between_points(other.p1, other.p2, p) and g.point_between_points(p1, p2, p):
                return True

        return False
