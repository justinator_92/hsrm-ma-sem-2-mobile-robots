from geometry import Point, Line
from objects import Triangle
from utils import GeometryUtils as g

import math
import time


COLOR = "#d1d1d1"
COLOR_OBSTACLE_DETECTED = "#ff0000"


class Sensor(Triangle):

    def __init__(self, origin, distance, ratio, canvas, root):

        e = distance * math.tan((ratio / 2.) * math.pi / 180.)
        p1 = Point(origin.x - e, origin.y - distance)
        p2 = Point(origin.x + e, origin.y - distance)

        Triangle.__init__(self, origin, p1, p2, COLOR)

        self.obstacles = []
        self.canvas = canvas
        self.root = root

        self.id = None

    def translate(self, vector):

        self.a += vector
        self.b += vector
        self.c += vector

        self.center += vector

        self.canvas.move(self.id, int(vector.x), int(vector.y))
        self.root.update()

    def draw(self):
        points = [self.a.x, self.a.y, self.b.x, self.b.y, self.c.x, self.c.y]
        self.id = self.canvas.create_polygon(points, fill=self.color)
        self.canvas.tag_raise(self.id)

    def rotate_sensor(self, angle, robot_center):

        self.a = g.rotate_around_point(self.a, robot_center, angle)
        self.b = g.rotate_around_point(self.b, robot_center, angle)
        self.c = g.rotate_around_point(self.c, robot_center, angle)

        self.center = g.rotate_around_point(self.center, robot_center, angle)

        points = [self.a.x, self.a.y, self.b.x, self.b.y, self.c.x, self.c.y]

        self.canvas.coords(self.id, *[int(x) for x in points])

    def add_obstacle(self, obj):
        self.obstacles.append(obj)

    def clear_obstacles(self):
        self.obstacles = []

    def take_measurement(self):

        points = [self.a, self.b, self.c, self.a]

        for obj in self.obstacles:

            for p1, p2 in zip(points, points[1:]):

                line = Line(p1, p2)

                if obj.calculate_intersection(line):
                    self.canvas.itemconfig(self.id, fill=COLOR_OBSTACLE_DETECTED)
                    self.root.update()

                    time.sleep(0.1)
                    self.canvas.itemconfig(self.id, fill=COLOR)
                    self.root.update()

                    return True

        return False
