from geometry import Point, Vector
from objects import Circle
from pathfinder import CRSpline, Pathfinder
from pathfinder import Node, PointGrid
from utils import GeometryUtils as g

import time
import Sensor


ROBOT_SIZE = 10


class Robot(Circle):

    def __init__(self, p1, color, canvas, root, width, height, objects):
        Circle.__init__(self, p1, ROBOT_SIZE, color)

        self.objects = objects

        # calculate minkowski sum for all obstacles...
        for obj in self.objects:
            obj.minkowski(self)
            obj.draw(canvas)

        self.root = root

        self.width = width
        self.height = height

        self.orientation = Vector(0, -1)  # current direction
        self.canvas = canvas
        self.id = None

        self.grid = None
        self.path_finder = None

        self.spline = CRSpline(0, 0)

        self.sensor = Sensor.Sensor(Point(p1.x, p1.y - ROBOT_SIZE), 100, 45, canvas, root)

        self.grid = PointGrid(self.canvas, self.root, self.width, self.height, self, self.move_to_goal, objects)

        self.grid.calculate_triangulation(self.objects)
        self.path_finder = Pathfinder(self.grid.points)

        self.spline_objects = set()
        self.DrawSpline = True

        self.draw()

    def make_new_grid(self):

        self.delete_spline()
        self.grid.calculate_triangulation(None)
        self.path_finder.reset_nodes(self.grid.points)

    def translate_robot(self, vector):
        self.center += vector
        self.sensor.translate(vector)

        self.canvas.move(self.id, int(vector.x), int(vector.y))
        self.root.update()

    def find_closest_point(self):

        min_distance = float('inf')
        ret_object = None

        for point in self.grid.points:
            length = (self.center - point).length()

            if length < min_distance:
                min_distance = length
                ret_object = point

        return ret_object

    def rotate_robot(self, angle):

        self.orientation = g.rotate_vector_origin(self.orientation, angle)
        self.sensor.rotate_sensor(angle, self.center)

        self.canvas.coords(self.id, (self.center.x - ROBOT_SIZE, self.center.y - ROBOT_SIZE,
                                     self.center.x + ROBOT_SIZE, self.center.y + ROBOT_SIZE))

        self.root.update()

    def move_robot(self, point):

        if self.center == point:  # do not move when robot is already on location
            return

        translate = (point - self.center)  # calculate vector to point
        angle = g.get_orientation(self.orientation, translate)

        self.rotate_robot(angle)
        self.translate_robot(translate)

        time.sleep(0.016)  # 60 fps per second...

    def move_straight_to_node(self, node):

        trans = (node - self.center) / 10.
        for e in range(0, 11):
            p = self.center + trans
            self.move_robot(p)

    def update_costs(self, node_a, node_b):

        node_a.add_neighbour_costs(node_b, float('inf'))
        node_b.add_neighbour_costs(node_a, float('inf'))
        self.path_finder.update_vertex(node_b)
        self.path_finder.update_vertex(node_a)

    def move_spline(self, path):

        assert len(path) >= 1

        if len(path) <= 2:
            for node in path:
                self.move_straight_to_node(node)
            return

        # calculating delta x...
        start_1, start_2 = path[0], path[1]
        end_1, end_2 = path[len(path) - 2], path[len(path) - 1]

        self.spline.start_derivative = (start_2.x - start_1.x) / (start_2.y - start_1.y)
        self.spline.end_derivative = (end_2.x - end_1.x) / (end_2.y - end_1.y)

        spline = self.spline.create_spline(path)

        if self.DrawSpline:
            self.draw_spline(spline)

        self.canvas.tag_raise(self.id)

        for node in path[:-1]:  # move until the n-1 point...

            next_node = path[path.index(node) + 1]

            angle = g.get_orientation(self.orientation, next_node - self.center)
            self.rotate_robot(angle)

            if self.sensor.take_measurement():
                index = path.index(node)

                if index <= len(path) - 1:
                    next_node = path[index + 1]
                    self.update_costs(node, next_node)

                return

            for p in spline[node]:
                self.move_robot(p)

    def move_to_goal(self, event, goal):

        node = self.find_closest_point()
        self.move_straight_to_node(node)

        self.path_finder.dstar_search(node, goal)

        while (self.center - goal).length() > 0.5:

            self.delete_spline()

            self.path_finder.compute_shortest_path()
            path = self.path_finder.construct_path(self.find_closest_point())  # construct path from closest point

            if not path:
                print "no path found"
                return

            self.move_spline(path)

        print "terminated..."

    def __sub__(self, other):
        return Point.__sub__(self.center, other)

    def draw(self):

        self.id = super(Robot, self).draw(self.canvas)
        self.canvas.tag_raise(self.id)

        self.sensor.draw()

    def __repr__(self):
        return "ROBOT: [point=%s, orientation=%s]" % (self.center, self.orientation)

    def draw_spline(self, curve):

        for nodes in curve.values():
            for p1, p2 in zip(nodes, nodes[1:]):
                c_id = self.canvas.create_line(p1.x, p1.y, p2.x, p2.y, fill="#000000", width=2)

                self.spline_objects.add(c_id)

            #    c_id = self.canvas.create_oval(math.ceil(p.x - 1), math.ceil(p.y - 1),
            #                                   math.ceil(p.x + 1), math.ceil(p.y + 1),
            #                                   fill="#000000", outline="#000000")
                #self.canvas.tag_lower(c_id)

    def delete_spline(self):

        for element in self.spline_objects:
            self.canvas.delete(element)

        self.spline_objects.clear()
