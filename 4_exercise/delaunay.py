from scipy.spatial import Delaunay
import numpy as np
import random

ppoints = []

for _ in range(100):
    ppoints.append([random.randint(0, 400), random.randint(0, 400)])


points = np.array(ppoints)
tri = Delaunay(points)

# We can visualize it:

import matplotlib.pyplot as plt
plt.triplot(points[:,0], points[:,1], tri.simplices.copy())
plt.plot(points[:,0], points[:,1], 'o')

# And add some further decorations:


plt.xlim(-0.5, 400); plt.ylim(-0.5, 400)
plt.show()