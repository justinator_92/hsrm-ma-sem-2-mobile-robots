import numpy as np
import matplotlib.pyplot as plt


def matrix(t):
    return np.matrix([[1, 0, t, 0, t**2/2, 0],
                      [0, 1, 0, t, 0, t**2/2],
                      [0, 0, 1, 0, t, 0],
                      [0, 0, 0, 1, 0, t],
                      [0, 0, 0, 0, 1, 0],
                      [0, 0, 0, 0, 0, 1]])


if __name__ == '__main__':


    print matrix(32)
    plt.plot([x for x in range(0, 22)], [0.1 * x**3 - 2 * x**2 + x + 50 for x in range(0, 22)], "r")

    plt.show()
