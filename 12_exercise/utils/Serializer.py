import pickle


class Serializer(object):

    def __init__(self):
        pass


    def serialize(self, obj, fileName):
        """
        Serialize a given object using pickle
        :param obj: object to serialize
        :param fileName: fileName to save object
        """

        fileObject = open(fileName, 'wb')
        pickle.dump(obj, fileObject)
        fileObject.close()


    def deserialize(self, fileName):
        """
        Deserialize an object using pickle
        :param fileName: filename pointing to file
        :return: deserialized object or None
        """

        fileObject = open(fileName, 'r')
        b = pickle.load(fileObject)

        return b
