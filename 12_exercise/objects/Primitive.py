from geometry import Point, Line, Vector
from pathfinder import ConvexHull

import BoudingSphere


class Primitive:
    """
    Base class for Primitive objects...
    """

    def __init__(self):
        """ constructor for Primitive geometric object """

        self.points = []
        self.min = []
        self.boundingBox = None

    def minkowski(self, obj):
        """ calculates Minkowski sum """

        points = set()
        progress, count = len(self.points) / 10, 0

        for pointA in self.points:
            if count > progress:
                print "yes"
                count = 0
            else:
                count += 1
            for pointB in obj.points:
                vector = pointB - obj.center
                p = pointA + vector
                if p.x >= 0 and p.y >= 0:
                    points.add(p)

        self.min = ConvexHull.convex_hull(list(points)[::-1])
        self.min.append(self.min[0])  # close polygon...
        print "minkowski", self.min

        #print "nr of points in minkowski sum!", len(self.min)

        #print "minimum: ", min(self.min)
        # calculating bounding sphere...
        upperleft = Point(min([p.x for p in self.min]), min([p.y for p in self.min]))
        lowerright = Point(max([p.x for p in self.min]), max([p.y for p in self.min]))
        #print upperleft, lowerright
        #print self.center

        l1 = (self.center - upperleft).length()
        l2 = (self.center - lowerright).length()

        self.boundingBox = BoudingSphere.BoundingSphere(self.center, max(l1, l2))

    def line_intersection(self, other_line, node):
        """
        Calculate intersection with line and this object
        :param node:
        :param other_line:
        :return:
        """
        for a, b in zip(self.min, self.min[1:]):
            line = Line(a, b)
            intersection = other_line.intersect_lines(line)

            print intersection

            if intersection:
                if node.point_in_polygon(intersection, 0):  # check if intersection is in own minkowski sum
                    return True  # line has intersection with other

        return False

    def intersect_with_node(self, node):
        """ test if line intersects with this object """

        for line in node.lines:

            #if self.point_in_polygon(line.p1, 0) or self.point_in_polygon(line.p2, 0):
            #    return True

            if self.line_intersection(line, node):
                return True

            # intersection = self.boundingBox.has_intersection(line.p1, line.p2)
            #
            # if not intersection:  # use bounding box for first check
            #     return False
            #
            # return self.line_intersection(line)

        return False

    def point_in_polygon(self, point, tolerance=0):
        """ test if point in convex polygon minkowski sum """

        for (p, q) in zip(self.min, self.min[1:]):
            v = (q - p)
            n = Vector(v.y, v.x).normalized()
            # n = v.normal().normalized()
            d = n.dot(point - p)
            if d < tolerance:  # changed because we are going counter clockwise... point in polygon...
                return False
        return True

    def draw_minkowski(self, canvas):
        """ draw minkowski sum to canvas """
        if len(self.min) > 0:
            return canvas.create_polygon([[p.x, p.y] for p in self.min], fill="#80C2FF", outline=None)
