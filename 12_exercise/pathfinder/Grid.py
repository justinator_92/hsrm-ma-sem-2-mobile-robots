from geometry import Line, Point, Vector
from pathfinder import Node

COLUMNS = 12
ROWS = 12

OCCUPIED = -1
FREE = 0


class Grid(object):

    def __init__(self, canvas, width, height, objects):

        self.canvas = canvas  # canvas to draw objects
        self.width = width  # width of canvas
        self.height = height  # height of canvas
        self.objects = objects  # object of scenes

        node_width = self.width / COLUMNS
        node_height = self.height / ROWS

        self.grid = [[e for e in range(COLUMNS)] for _ in range(ROWS)]

        for column in range(0, COLUMNS):
            for row in range(0, ROWS):
                node = Node.Node(column, row, node_width, node_height)
                self.grid[row][column] = node
                #print node
                node.draw(canvas, self.node_clicked)

        self.start = None
        self.end = None

        self.selection = 0
        self.lines = []
        self.change_distance()

    def node_clicked(self, event, node):
        print "clicked", event, node

        if node.distance == OCCUPIED:
            return

        # select start node first
        if self.selection == 0:
            self.reset_nodes()
            self.start, self.end = node, None
            self.start.redraw_cell(self.canvas, "#ffff00")
        else:  # select end node.. start pathfinding
            self.end = node
            self.chamfer()
            self.end.redraw_cell(self.canvas, "#ff0000")

        self.selection = (self.selection + 1) % 2

    def reset_nodes(self):

        for line in self.lines:
            self.canvas.delete(line)

        for column in range(0, COLUMNS):
            for row in range(0, ROWS):
                node = self.grid[row][column]
                node.change_distance(self.canvas, 0)
                node.redraw_cell(self.canvas, '#ffffff')

        self.change_distance()

    def change_distance(self):

        self.grid[2][2].change_distance(self.canvas, OCCUPIED)
        self.grid[3][2].change_distance(self.canvas, OCCUPIED)
        self.grid[4][2].change_distance(self.canvas, OCCUPIED)
        self.grid[5][2].change_distance(self.canvas, OCCUPIED)
        self.grid[6][2].change_distance(self.canvas, OCCUPIED)
        self.grid[7][2].change_distance(self.canvas, OCCUPIED)
        self.grid[8][2].change_distance(self.canvas, OCCUPIED)
        self.grid[9][2].change_distance(self.canvas, OCCUPIED)

        self.grid[2][3].change_distance(self.canvas, OCCUPIED)
        self.grid[3][3].change_distance(self.canvas, OCCUPIED)
        self.grid[4][3].change_distance(self.canvas, OCCUPIED)
        self.grid[5][3].change_distance(self.canvas, OCCUPIED)
        self.grid[6][3].change_distance(self.canvas, OCCUPIED)
        self.grid[7][3].change_distance(self.canvas, OCCUPIED)
        self.grid[8][3].change_distance(self.canvas, OCCUPIED)
        self.grid[9][3].change_distance(self.canvas, OCCUPIED)

        self.grid[1][6].change_distance(self.canvas, OCCUPIED)
        self.grid[1][7].change_distance(self.canvas, OCCUPIED)
        self.grid[1][8].change_distance(self.canvas, OCCUPIED)
        self.grid[2][6].change_distance(self.canvas, OCCUPIED)
        self.grid[2][7].change_distance(self.canvas, OCCUPIED)
        self.grid[2][8].change_distance(self.canvas, OCCUPIED)
        self.grid[3][6].change_distance(self.canvas, OCCUPIED)
        self.grid[3][7].change_distance(self.canvas, OCCUPIED)
        self.grid[3][8].change_distance(self.canvas, OCCUPIED)
        self.grid[4][6].change_distance(self.canvas, OCCUPIED)
        self.grid[4][7].change_distance(self.canvas, OCCUPIED)
        self.grid[4][8].change_distance(self.canvas, OCCUPIED)

        self.grid[5][5].change_distance(self.canvas, OCCUPIED)
        self.grid[5][6].change_distance(self.canvas, OCCUPIED)
        self.grid[5][7].change_distance(self.canvas, OCCUPIED)
        self.grid[5][8].change_distance(self.canvas, OCCUPIED)
        self.grid[5][9].change_distance(self.canvas, OCCUPIED)

        self.grid[6][5].change_distance(self.canvas, OCCUPIED)
        self.grid[6][6].change_distance(self.canvas, OCCUPIED)
        self.grid[6][7].change_distance(self.canvas, OCCUPIED)
        self.grid[6][8].change_distance(self.canvas, OCCUPIED)
        self.grid[6][9].change_distance(self.canvas, OCCUPIED)

        self.grid[7][5].change_distance(self.canvas, OCCUPIED)
        self.grid[7][6].change_distance(self.canvas, OCCUPIED)
        self.grid[7][7].change_distance(self.canvas, OCCUPIED)
        self.grid[7][8].change_distance(self.canvas, OCCUPIED)
        self.grid[7][9].change_distance(self.canvas, OCCUPIED)

        self.grid[8][5].change_distance(self.canvas, OCCUPIED)
        self.grid[8][6].change_distance(self.canvas, OCCUPIED)
        self.grid[8][7].change_distance(self.canvas, OCCUPIED)
        self.grid[8][8].change_distance(self.canvas, OCCUPIED)
        self.grid[8][9].change_distance(self.canvas, OCCUPIED)

        self.grid[9][5].change_distance(self.canvas, OCCUPIED)
        self.grid[9][6].change_distance(self.canvas, OCCUPIED)
        self.grid[9][7].change_distance(self.canvas, OCCUPIED)
        self.grid[9][8].change_distance(self.canvas, OCCUPIED)
        self.grid[9][9].change_distance(self.canvas, OCCUPIED)

    def chamfer(self):

        # calculating values for matrix...

        for column in range(0, COLUMNS):
            for row in range(0, ROWS):
                distance = max(abs(self.start.x - column), abs(self.start.y - row))
                #distance = abs(self.start.x - column) + abs(self.start.y - row)
                self.grid[row][column].change_distance(self.canvas, distance)

        # backtracking values... start with end point
        path = []
        closed_list = []
        self.find_path(self.end, self.start, path, closed_list)
        print "closed list: ", closed_list
        path.append(self.end)
        print "path", path

        for node in path:
            node.redraw_cell(self.canvas, "#00ffff")
        self.make_polygon(path)

    def make_polygon(self, path):

        for node1, node2 in zip(path, path[1:]):
            self.lines.append(self.canvas.create_line(node1.start_x + node1.width / 2, node1.start_y + node1.height / 2,
                                                      node2.start_x + node2.width / 2, node2.start_y + node2.height / 2,
                                                      fill="black", width=3))

    def get_neighbours(self, current):

        neighbours = []

        x, y = current.x, current.y

        for col in range(-1, 2):
            for row in range(-1, 2):
                if 0 <= x + col < COLUMNS and 0 <= y + row < ROWS:

                    if col == 0 and row == 0:
                        continue

                    neighbours.append(self.grid[y+row][x+col])

        return neighbours

    def find_path(self, current, start, node_list=[], closed_list=[]):
        """
        find path in graph using backtracking algorithm
        :return:
        """

        for neighbour in self.get_neighbours(current):

            # check if neighbour already visited...
            if neighbour in closed_list:
                #print "already visited... ", neighbour
                continue

            closed_list.append(neighbour)

            if neighbour.distance == OCCUPIED:
                continue

            if neighbour == start:
                print "start node reached!"
                node_list.append(neighbour)
                return True  # found start node

            #            if neighbour.distance == current.distance - 1:  # find exact linear equation...
            if neighbour.distance <= current.distance:  # find exact linear equation...
                # print "good neighbour: ", neighbour
                if not self.find_path(neighbour, start, node_list, closed_list):
                    continue
                else:
                    node_list.append(neighbour)
                    return True
        return False
