from geometry import Point, Line
from objects import Rectangle, Circle
from pathfinder import Grid
from pathfinder import Node


BLUE = "#0080FF"  # light blue
GREEN = "#FF0000"  # green
LIGHTBLUE = "#80C2FF"  # even lighter blue
BLACK = "#000000"  # black
RED = "#FF0000"  # red

SIZE = 10

FRAME = 4  # size of frame


class Space:

    def __init__(self, canvas, img, width, height):
        """
        Constructor for Space Class. This is class the mastermind that manages
        the entire application.
        :param canvas: the canvas to draw
        :param img: image to place pixels
        :param width: width of the GUI
        :param height: height of the GUI
        """

        # create robot...
        robot = Rectangle(Point(300, 300), Point(315, 315), RED)

        # create obstacles...
        o1 = Circle(Point(250, 100), 40, BLUE)
        o2 = Rectangle(Point(80, 100), Point(120, 300), BLUE)
        o3 = Rectangle(Point(200, 200), Point(300, 300), BLUE)

        self.objects = [o1, o2, o3]  # , o2, o3]

        for obj in self.objects:
            obj.minkowski(robot)

#        node = Node.Node(2, 2, 33, 33)

#        print "intersect with node: ", o2.intersect_with_node(node)

        self.grid = Grid.Grid(canvas, width, height, self.objects)
        self.canvas = canvas  # the canvas to draw all objects
        self.img = img  # image to set pixel manually on GUI

        self.draw_objects(self.objects)

    def draw_objects(self, objs):
        """
        draws a list of objects
        :param objs: objects to draw
        :return: None
        """
        for obj in objs:
            print obj.draw(self.canvas)
