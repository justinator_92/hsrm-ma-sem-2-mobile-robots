from geometry import Line, Point, Vector

COLUMNS = 12
ROWS = 12

OCCUPIED = -1
FREE = 0


class Node(object):

    def __init__(self, x, y, width, height):
        self.state = FREE
        self.x = x
        self.y = y

        self.start_x = x * width
        self.start_y = y * height

        self.width = width
        self.height = height

        self.distance = 0

        self.obj_id = None
        self.text_id = None

        self.lines = [Line(Point(self.start_x, self.start_y), Point(self.start_x + self.width, self.start_y)),
                      Line(Point(self.start_x, self.start_y), Point(self.start_x, self.start_y + self.height)),
                      Line(Point(self.start_x, self.start_y + self.height), Point(self.start_x + self.width, self.start_y + self.height)),
                      Line(Point(self.start_x + self.width, self.start_y), Point(self.start_x + self.width, self.start_y + self.height))]

        self.points = [Point(self.start_x, self.start_y),
                       Point(self.start_x, self.start_y + self.height),
                       Point(self.start_x + self.width, self.start_y + self.height),
                       Point(self.start_x + self.width, self.start_y),
                       Point(self.start_x, self.start_y)]

    def draw(self, canvas, callback):
        self.obj_id = canvas.create_rectangle(self.start_x, self.start_y,
                                              self.start_x+self.width, self.start_y+self.height,
                                              fill=None, outline="#000000", tags=str(self.x))
        self.text_id = canvas.create_text(self.start_x + self.width / 2, self.start_y + self.height / 2,
                                          text=str(self.distance), tags=str(self.x))

        # set callbacks to text and node...
        canvas.tag_bind(self.obj_id, '<ButtonPress-1>', lambda event, arg=self: callback(event, arg))
        canvas.tag_bind(self.text_id, '<ButtonPress-1>', lambda event, arg=self: callback(event, arg))

    def redraw_cell(self, canvas, color):
        canvas.itemconfigure(self.obj_id, fill=color)

    def change_distance(self, canvas, distance):

        if self.distance == OCCUPIED:
            return

        self.distance = distance
        canvas.itemconfigure(self.text_id, text=str(self.distance))

    def point_in_polygon(self, intersection_point, tolerance=0):
        """ test if point in convex polygon minkowski sum """

        points = self.points

        for (p, q) in zip(points, points[1:]):
            v = (q - p)
            n = Vector(-v.y, v.x).normalized()
            # n = v.normal().normalized()
            d = n.dot(intersection_point - p)
            if d > 0:  # changed because we are going counter clockwise... point in polygon...
                return False
        return True

    def __repr__(self):
        return "Node: [x=%f, y=%f, w=%f, h=%f, d = %f]\n" % (self.start_x, self.start_y, self.width, self.height, self.distance)
