from Tkinter import *
from Canvas import *

import sys

WIDTH = 400  # width of canvas
HEIGHT = 400  # height of canvas

HPSIZE = 1  # half of point size (must be integer)
COLORA = "#0080FF"  # light blue
COLORB = "#000000"  # green
COLORC = "#80C2FF"  # even lighter blue

SCALE = 1000  # scale factor


class Curve:

    def __init__(self, a, b, c, d, x):
        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.x = x

    def pointAtParameter(self, x1):
        return self.a + self.b * (x1 - self.x) + self.c * (x1 - self.x) ** 2 + self.d * (x1 - self.x) ** 3

    def __repr__(self):
        return "Curve: [a=%f, b=%f, c=%f, d=%f, x=%f]\n" % (self.a, self.b, self.c, self.d, self.x)

# (control-)points
POINTS = [(2., 3.), (4., 9.), (6., 2.), (8., 6.), (10., 3.), (12., 9.), (14., 4.)]


def drawPoints(p, color=COLORB):
    """ draw points """
    element = can.create_oval(p[0]-HPSIZE, p[1]-HPSIZE, p[0]+HPSIZE, p[1]+HPSIZE, fill=color, outline=color)


def quit(root=None):
    """ quit programm """
    if not root:
        sys.exit(0)
    root._root().quit()
    root._root().destroy()


def calculate(p, b0=0, c0=0):
    """ calculate cubic spline curve """

    curves = []

    # laut vorlesung: ais = yis
    x0, x1 = p[0][0], p[1][0]
    a0, a1 = p[0][1], p[1][1]

    d0 = -(b0 / (x1 - x0)**2) + ((a1 - a0) / (x1 - x0)**3) + (c0 / (x1 - x0)) # 0.75

    curve = Curve(a0, b0, c0, d0, x0)
    curves.append(curve)

    for e in range(0, len(p) - 2):
        x0, x1, x2 = p[e][0], p[e + 1][0], p[e + 2][0]
        a0, a1, a2 = p[e][1], p[e + 1][1], p[e + 2][1]

        c1 = 3 * d0 * (x1 - x0) + c0

        b1 = (a1 - a0) / (x1 - x0) + 2 * d0 * (x1 - x0) ** 2 + c0 * (x1 - x0)

        R = 3 * ((a2 - a1) / (x2 - x1) - (a1 - a0) / (x1 - x0))
        c2 = R / (x2 - x1) - 2 * c1 * ((x2 - x0) / (x2 - x1)) - c0 * (x1 - x0) / (x2 - x1)

        d1 = (c2 - c1) / (3 * (x2 - x1))

        curve = Curve(a1, b1, c1, d1, x1)
        curves.append(curve)

        c0 = c1
        d0 = d1
        a0 = a1
        b0 = b1
        c1 = c2

    return curves


def paint(curves):

    curves.append(curves[0])
    print curves

    scale = 100.

    for current, next in zip(curves, curves[1:]):
        distance = next.x - current.x

        for step in range(0, int(distance * scale + 1)):
            x = current.x + step / scale
            p = current.pointAtParameter(x)
            drawPoints((x, HEIGHT/2-p))

if __name__ == "__main__":

    # create main window
    mw = Tk()

    # create and position canvas and buttons
    cFr = Frame(mw, width=WIDTH, height=HEIGHT, relief="sunken", bd=1)
    cFr.pack(side="top")
    can = Canvas(cFr, width=WIDTH, height=HEIGHT)
    can.pack()
    cFr = Frame(mw)
    cFr.pack(side="left")
    eFr = Frame(mw)
    eFr.pack(side="right")
    bExit = Button(eFr, text="Quit", command=(lambda root=mw: quit(root)))
    bExit.pack()

    POINTS = [(x * 10, y * 0.3) for x, y in POINTS]

    curves = calculate(POINTS)
    paint(curves)

    mw.mainloop()
