import numpy as np

RESOLUTION = 20.


class CatmullRomSpline(object):

    def __init__(self, start_derivative, end_derivative, scale):
        """ constructor for catmull rom splines curve """
        self.start = self.create_start()
        self.middle = self.create_middle()
        self.end = self.create_end()

        self.start_derivative = start_derivative
        self.end_derivative = end_derivative

        self.scale = scale

    def create_middle(self, a=1):
        """ create start matrix """
        return np.matrix([[0, 2, 0, 0],
                            [-a, 0, a, 0],
                            [2*a, -6+a, 6-2*a, -a],
                            [-a, 4-a, -4+a, a]])

    def create_start(self):
        """ create matrix for middle points """
        return np.matrix([[0, 2, 0, 0],
                            [2, 0, 0, 0],
                            [-4, -5, 6, -1],
                            [2, 3, -4, 1]])

    def create_end(self):
        """ create matrix for last point """
        return np.matrix([[0, 2, 0, 0],
                          [-1, 0, 1, 0],
                          [2, -6, 4, -2],
                          [-1, 4, -3, 2]])

    def calculate_first_segment(self, p0, p1, p2):

        point_list = []
        a = 0.5

        for e in range(0, int(RESOLUTION)):

            t = e / RESOLUTION
            b = np.array([[1, t, t*t, t*t*t]])
            c = self.start
            d = np.array([[1, self.start_derivative], p0, p1, p2])
            p = a * b.dot(c.dot(d))

            point_list.append(p)

        return point_list

    def calculate_middle_segment(self, p0, p1, p2, p3):

        point_list = []
        a = 0.5

        for e in range(0, int(RESOLUTION)):

            t = e / RESOLUTION
            b = np.array([[1, t, t*t, t*t*t]])
            c = self.middle
            d = np.array([p0, p1, p2, p3])
            p = a * b.dot(c.dot(d))

            point_list.append(p)

        return point_list

    def calculate_end_segment(self, p0, p1, p2):
        """ calculate end segment of spline """

        point_list = []

        for e in range(0, int(RESOLUTION) + 1):

            t = e / RESOLUTION
            b = np.array([[1, t, t*t, t*t*t]])

            d = np.array([p0, p1, p2, [1, self.end_derivative]])
            a = self.create_end().dot(d)
            temp = b.dot(a)

            p = 0.5 * temp
            point_list.append(p)

        return point_list

    def create_spline(self, point_list):
        """ create spline curve """

        ret = []

        for i in range(0, len(point_list)-1):

            if i == 0:  # start point
                ret.extend(self.calculate_first_segment(point_list[i], point_list[i+1], point_list[i+2]))
            elif i == len(point_list)-2:  # end points
                ret.extend(self.calculate_end_segment(point_list[i-1], point_list[i], point_list[i+1]))
            else:  # regular middle point
                p0, p1, p2, p3 = point_list[i-1], point_list[i], point_list[i+1], point_list[i+2]
                ret.extend(self.calculate_middle_segment(p0, p1, p2, p3))

        return ret


if __name__ == '__main__':

#	print create_middle(1)
#	print create_start()
#	print create_end()

    points = [[20, 5], [22, 12], [34, 17], [34, 24], [22, 36], [22, 25],[18, 20], [12, 20], [17, 17], [16, 10]]

    points = [np.array(p) for p in points]

    spline = CatmullRomSpline()
    spline.create_spline(points)
