from Tkinter import *
from Canvas import *
from catsplines import CatmullRomSpline
import numpy as np

WIDTH = 400  # width of canvas
HEIGHT = 400  # height of canvas

HPSIZE = 0.5  # half of point size (must be integer)
COLORA = "#0080FF"  # light blue
COLORB = "#FF0000"  # green
COLORC = "#80C2FF"  # even lighter blue

pointList = []   # list of points

SCALE = 20  # scale factor of spline curve


def drawPoints(points, color):
    """ draw (control-)points """
    for p in points:
        a, b = p.item((0, 0)) * SCALE, p.item((0, 1)) * SCALE
        element = can.create_oval(a-HPSIZE, HEIGHT-b-HPSIZE, a+HPSIZE, HEIGHT-b+HPSIZE, fill=color, outline=color)


def quit(root=None):
    """ quit programm """
    if root==None:
        sys.exit(0)
    root._root().quit()
    root._root().destroy()


def draw():
    """ draw spline curve """

    #points = [[16, 10], [20, 5], [22, 12], [34, 17], [34, 24], [22, 36], [22, 25], [18, 20], [12, 20], [17, 17], [16, 10], [20, 5], [22, 12]]
    points = [[2, 3], [4, 9], [6, 2], [8, 6], [10, 3], [12, 9], [14, 4]]

    spline = CatmullRomSpline(-10, 10, SCALE)
    points = spline.create_spline(points)

    drawPoints(points, COLORA)


if __name__ == "__main__":
    

    # create main window
    mw = Tk()

    # create and position canvas and buttons
    cFr = Frame(mw, width=WIDTH, height=HEIGHT, relief="sunken", bd=1)
    cFr.pack(side="top")
    can = Canvas(cFr, width=WIDTH, height=HEIGHT)
    can.pack()
    cFr = Frame(mw)
    cFr.pack(side="left")
    eFr = Frame(mw)
    eFr.pack(side="right")
    bExit = Button(eFr, text="Quit", command=(lambda root=mw: quit(root)))
    bExit.pack()

    draw()

    mw.mainloop()
    
