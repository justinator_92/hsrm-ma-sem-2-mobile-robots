import os
import math
parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.sys.path.insert(0,parentdir) 
from geometry import Point
from geometry import Vector

class Rectangle:

    def __init__(self, p1, p2):
        self.p0 = p1 # lower left
        self.p1 = Point(p1.a, p2.b) # upper left
        self.p2 = p2 # upper right
        self.p3 = Point(p2.a, p1.b) # lower right

        trans = p2 - p1
        #print trans

        center = Point(trans.a / 2, trans.b / 2)
        #print center
        self.center = center + Vector(p1.a, p1.b)
        #print self.center


#        self.center = Vector((p2.b - p1.b) / 2. + p1.a, (p2.b - p1.b) / 2. + p1.b)

        # print "center: %s" % self.center

        # print self.p0
        # print self.p1
        # print self.p2
        # print self.p3


    def rotatePoint(self, point, angle):
        """ rotate a point around origin with given angle """
        t = point - self.center # translate point to origin

        # print "translated: %s" % t

        rot = Point(t.a * math.cos(angle) - t.b * math.sin(angle), t.a * math.sin(angle) + t.b * math.cos(angle))
        
        # print "rotated: %s" % rot

        tb = rot + self.center # translate point back

        # print "translated back: %s" % tb
        return tb


    def rotate(self, angle):
        """ rotate the rectangle around angle """

        ang = math.radians(angle)

        self.p0 = self.rotatePoint(self.p0, ang)
        self.p1 = self.rotatePoint(self.p1, ang)
        self.p2 = self.rotatePoint(self.p2, ang)
        self.p3 = self.rotatePoint(self.p3, ang)

        # print self.p0
        # print self.p1
        # print self.p2
        # print self.p3
        
    def pointInPolygon(self, point):
        """ test if point in convex polygon """

        polygon = [self.p0, self.p1, self.p2, self.p3, self.p0]

        for(p, q) in zip(polygon, polygon[1:]):
            v = (q-p)
            n = v.normal().normalized()
            d = n.dot(point - p)

            if d > 0:
                return False
        return True


    def draw(self):
        """ draws the rectangle """

        points = []

        for x in range(400):
            for y in range(400):
                p = Point(x, y)
                if(self.pointInPolygon(p)):
                    points.append(p)

        return points




if __name__ == '__main__':
    rect = Rectangle(Point(300, 300), Point(323, 371))
    