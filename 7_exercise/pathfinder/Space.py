from geometry import Point, Line
from objects import Circle
from objects import Rectangle
from utils import Serializer
from scipy.spatial import Delaunay
from pathfinder import Node, astar

import random
import numpy as np

BLUE = "#0080FF"  # light blue
GREEN = "#FF0000"  # green
LIGHTBLUE = "#80C2FF"  # even lighter blue
BLACK = "#000000"  # black
RED = "#FF0000"  # red

SIZE = 2

FRAME = 4  # size of frame

ONE, TWO = 1, 2


class Space:

    def __init__(self, canvas, img, width, height):
        """ constructor for space object """

        self.grid_points = set()
        self.serializer = Serializer()
        self.canvas = canvas
        self.img = img

        self.width = width
        self.height = height

        self.lines = set()

        self.graph = {}

        robot = Rectangle(Point(300, 300), Point(306, 306), RED)

        # create obsticals...
        o1 = Circle(Point(500, 300), 50, BLUE)
        o2 = Rectangle(Point(50, 50), Point(101, 151), BLUE)
        #o3 = Rectangle(Point(600, 100), Point(650, 400), BLUE)
        o4 = Rectangle(Point(50, 451), Point(150, 501), BLUE)

        # # create borders of environment...
        #b1 = Rectangle(Point(0, 0), Point(self.width, FRAME), BLUE)
        #b2 = Rectangle(Point(0, 0), Point(FRAME, self.height), BLUE)
        #b3 = Rectangle(Point(0, self.height - FRAME), Point(self.width, self.height), BLUE)
        #b4 = Rectangle(Point(self.width - FRAME, 0), Point(self.width, self.height), BLUE)

        self.objects = [o1, o2, o4]  # , o2, o3]  # o2, o3, o4, b1, b2, b3, b4]

        # calculate minkowski sum...

        for obj in self.objects:
            obj.minkowski(robot)

        self.start = Node(Point(200, 100))
        self.end = Node(Point(700, 400))

        object_id = self.canvas.create_oval(self.start.point.x-10, self.start.point.y-10,
                                            self.start.point.x+10, self.start.point.y+10, tags="event",
                                                    fill="#ff00ff", outline="#ff00ff")

        object_id = self.canvas.create_oval(self.end.point.x-10, self.end.point.y-10,
                                            self.end.point.x+10, self.end.point.y+10, tags="event",
                                                    fill="#00ffff", outline="#00ffff")

        self.path = []

        self.generate_points()
        self.drawObjects(self.objects)

        #self.load_configuration("/home/justin/Schreibtisch/robotic/project/src/Test.conf")

        self.path = set()


    def generate_points(self, amount=100):
        """
        generates random statistically fix points
        """

        for line in self.lines:
            self.canvas.delete(line)

        for fix in self.grid_points:
#            self.canvas.tag_unbind(fix, "<ButtonPress-1>")
            self.canvas.delete(fix)

        self.lines.clear()
        self.grid_points.clear()

        points = []

        while len(points) < amount:
            valid = True
            p = Point(random.randint(5, self.width), random.randint(5, self.height))
            for obj in self.objects:
                if obj.point_in_polygon(p):
                    valid = False
                    break

            if valid:
                node = Node(p)
                print "add node", node
                points.append(node)
                object_id = self.canvas.create_oval(p.x-SIZE, p.y-SIZE, p.x+SIZE, p.y+SIZE, tags="event",
                                                    fill="#ff0000", outline="#ff0000")

                self.grid_points.add(object_id)

        points.append(self.start)
        points.append(self.end)

        # TODO: replace with own triangulation...
        pp = np.array([[node.point.x, node.point.y] for node in points])
        tri = Delaunay(pp)

        self.graph.clear()

        for e in tri.simplices:
            node1 = points[e[0]]
            node2 = points[e[1]]
            node3 = points[e[2]]

            nodes = {Line(node1.point, node2.point): (node1, node2),
                     Line(node2.point, node3.point): (node2, node3),
                     Line(node3.point, node1.point): (node3, node1)}

            for line, values in nodes.iteritems():
                valid = True

                for obj in self.objects:
                    if obj.intersect_with_line(line):
                        valid = False
                        break

                if valid:  # connection between two points is valid. add node to graph..

                    distance = (values[0].point - values[1].point).length()  # calculate vector between points..

                    if not values[0] in self.graph:
                        self.graph[values[0]] = {values[1]: distance}
                    else:
                        self.graph[values[0]][values[1]] = distance

                    if not values[1] in self.graph:
                        self.graph[values[1]] = {values[0]: distance}
                    else:
                        self.graph[values[1]][values[0]] = distance

                    self.lines.add(self.canvas.create_line(line.p1.x, line.p1.y, line.p2.x, line.p2.y, fill="#cccccc"))

        print self.graph
        self.calculate_route()

    def calculate_route(self):

        end = astar(self.graph, self.start, self.end)

        if not end:
            print "no path found..."
            return

        print end
        path = [end]

        # draw path:
        while end.predecessor:
            path.append(end.predecessor)
            end = end.predecessor

        for start, end in zip(path, path[1:]):
            self.lines.add(self.canvas.create_line(start.point.x, start.point.y, end.point.x, end.point.y,
                                                  fill="#ff0000"))

    def load_configuration(self, filename):
        """
        Open a configuration model
        :param filename:
        :return:
        """

        obj = self.serializer.deserialize(filename)
        print obj

        if obj != None:
            self.objects = obj
            self.drawObjects(self.objects)

    def save_configuration(self, filename):
        """
        saves all objects to file
        :param filename: filename
        """
        self.serializer.serialize(self.objects, filename)

    def drawPoints(self, points, color):
        """ draw (control-)points """
        for p in points:
            self.drawPoint(p, color)

    def drawPoint(self, p, color):
        """ draw a point in canvas """
        self.img.put(color, (p.a, p.b))

    def drawObject(self, obj):
        """
        draws a object
        :param obj: object to draw
        :return: None
        """
        for point in obj.points:
            self.drawPoint(point, obj.color)

    def drawObjects(self, objs):
        """
        draws a list of objects
        :param objs: objects to draw
        :return: None
        """
        for obj in objs:
            print obj.draw(self.canvas)
