class Node(object):

    def __init__(self, point, g=float('inf')):
        self.f = 0
        self.g = g
        self.h = 0

        self.point = point
        self.predecessor = None  # pointer to previous Node

    def __repr__(self):
        return "Node [location: %f, %f]" % (self.point.x, self.point.y)
