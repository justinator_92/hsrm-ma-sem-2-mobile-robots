from geometry import Point, Line, Vector

import os
import math

from objects.Primitive import Primitive

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.sys.path.insert(0, parentdir)


class Rectangle(Primitive):

    def __init__(self, p1, p2, color):
        """ constructor for Rectangle """
        Primitive.__init__(self)
        self.p0 = p1  # lower left
        self.p1 = Point(p1.x, p2.y)  # upper left
        self.p2 = p2  # upper right
        self.p3 = Point(p2.x, p1.y)  # lower right

        size = p2 - p1  # width, height
        self.boundingBox = max(size.x, size.y)

        center = Point(size.x / 2, size.y / 2)
        self.center = center + Vector(p1.x, p1.y)

        self.color = color

        self.calculatePoints()  # calculate points
        print self

    def rotatePoint(self, point, angle):
        """ rotate a point around origin with given angle """
        t = point - self.center  # translate point to origin
        rot = Point(t.a * math.cos(angle) - t.b * math.sin(angle), t.a * math.sin(angle) + t.b * math.cos(angle))
        tb = rot + self.center  # translate point back
        return tb

    def rotate(self, angle):
        """ rotate the rectangle around angle """

        ang = math.radians(angle)

        self.p0 = self.rotatePoint(self.p0, ang)
        self.p1 = self.rotatePoint(self.p1, ang)
        self.p2 = self.rotatePoint(self.p2, ang)
        self.p3 = self.rotatePoint(self.p3, ang)
        
    def intersect(self, point):
        """ test if point in convex polygon """

        polygon = [self.p0, self.p1, self.p2, self.p3, self.p0]

        for(p, q) in zip(polygon, polygon[1:]):
            v = (q-p)
            n = v.normal().normalized()
            d = n.dot(point - p)

            if d > 0:
                return False
        return True

    def calculatePoints(self):
        """
        Calculate points
        :return:
        """
        for x in range(self.p0.x, self.p2.x + 1):
            for y in range(self.p0.y, self.p2.y + 1):

                p = Point(x, y)
                if self.intersect(p):
                    self.points.append(p)

    def draw(self, canvas):
        """
        draw shape on canvas
        :param canvas:
        :return:
        """
        points = [self.p0.x, self.p0.y, self.p1.x, self.p1.y,
                  self.p2.x, self.p2.y, self.p3.x, self.p3.y]

#        if(len(self.min) > 0):
#            canvas.create_polygon([item for sublist in self.min for item in sublist], fill="#ff00aa")
        self.draw_minkowski(canvas)
        return canvas.create_polygon(points, fill=self.color)
        #return 2

    def __repr__(self):
        """
        string representation of rectangle object
        :return:
        """
        return "Rect: [p0: %s, p2: %s, Points: %d, mink: %d]" % (self.p0, self.p2, len(self.points), len(self.min))

if __name__ == '__main__':
    rect = Rectangle(Point(300, 300), Point(310, 310), "#000000")
    robot = Rectangle(Point(400, 400), Point(402, 402), "#ff0000")

    rect.minkowski(robot)

    line = Line(Point(200, 305), Point(500, 305))

    print rect.intersect_line(line)
