from geometry import Point, Line
from pathfinder import convex_hull as convex_hull

import BoudingSphere


class Primitive:
    """
    Base class for Primitive objects...
    """

    def __init__(self):
        """ constructor for Primitive geometric object """

        self.points = []
        self.min = []
        self.boundingBox = None

    def minkowski(self, obj):
        """ calculates Minkowski sum """

        points = set()
        progress, count = len(self.points) / 10, 0

        for pointA in self.points:
            if count > progress:
                print "yes"
                count = 0
            else:
                count += 1
            for pointB in obj.points:
                vector = pointB - obj.center
                p = pointA + vector
                if p.x >= 0 and p.y >= 0:
                    points.add(p)

        self.min = convex_hull(list(points)[::-1])
        print "nr of points in minkowski sum!", len(self.min)

        print "minimum: ", min(self.min)
        # calculating bounding sphere...
        upperleft = Point(min([p.x for p in self.min]), min([p.y for p in self.min]))
        lowerright = Point(max([p.x for p in self.min]), max([p.y for p in self.min]))
        print upperleft, lowerright
        print self.center

        l1 = (self.center - upperleft).length()
        l2 = (self.center - lowerright).length()

        self.boundingBox = BoudingSphere.BoundingSphere(self.center, max(l1, l2))

    def line_intersection(self, other):
        """
        Calculate intersection with line and this object
        :param line:
        :return:
        """
        for a, b in zip(self.min, self.min[1:]):
            line = Line(a, b)
            intersection = other.intersect_lines(line)
            if intersection:
                if self.point_in_polygon(intersection, -5):  # check if intersection is in own minkowski sum
                    return True  # line has intersection with other

        return False

    def intersect_with_line(self, line):
        """ test if line intersects with this object """

        if self.boundingBox.point_in_sphere(line.p1) or self.boundingBox.point_in_sphere(line.p2):
            return self.line_intersection(line)

        intersection = self.boundingBox.has_intersection(line.p1, line.p2)

        if not intersection:  # use bounding box for first check
            return False

        return self.line_intersection(line)

    def point_in_polygon(self, point, tolerance=0):
        """ test if point in convex polygon minkowski sum """

        for (p, q) in zip(self.min, self.min[1:]):
            v = (q - p)
            n = v.normal().normalized()
            d = n.dot(point - p)
            if d < tolerance:  # changed because we are going counter clockwise...
                return False
        return True

    def draw_minkowski(self, canvas):
        """ draw minkowski sum to canvas """
        if len(self.min) > 0:
            return canvas.create_polygon([[p.x, p.y] for p in self.min], fill="#000000")
