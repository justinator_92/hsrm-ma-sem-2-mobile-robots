from Tkinter import *
from Canvas import *
from geometry import Point
from geometry import Vector
from objects import Circle
from objects import Quad
from objects import Rectangle
import math
import sys

WIDTH  = 400 # width of canvas
HEIGHT = 400 # height of canvas

HPSIZE = 2 # half of point size (must be integer)
COLORA = "#0080FF" # light blue
COLORB = "#FF0000" # green
COLORC = "#80C2FF" # even lighter blue

pointList = []   # list of points


def drawPoints(points, color):
    """ draw (control-)points """
    for p in points:
        element = can.create_oval(p.a-HPSIZE, p.b-HPSIZE, p.a+HPSIZE, p.b+HPSIZE, fill=color, outline=color)

def minowski(setA, setB, center):
    """ calculates minowski sum """
    points = set()

    for pointA in setA:
        for pointB in setB:
            vecB = pointB - center
            v = pointA + Vector(vecB.a, vecB.b)
            points.add(v)
            
    return points


def quit(root=None):
    """ quit programm """
    if root==None:
        sys.exit(0)
    root._root().quit()
    root._root().destroy()


def draw():
    """ draw elements """
    obj1 = Circle(Point(150, 250), 50)
    setA = obj1.draw()
    print "setA calculated. object has %d points" % (len(setA))

    obj2 = Rectangle(Point(280, 100), Point(320, 250))

    setB = obj2.draw()
    print "setB calculated. object has %d points" % (len(setB))
    
    obj3 = Rectangle(Point(70, 70), Point(130, 130))
    setC = obj3.draw()
    print "setC calculated. object has %d points" % (len(setC))
    
    # border...
    a = Rectangle(Point(0, 0), Point(400, 10))
    b = Rectangle(Point(0, 0), Point(10, 400))
    c = Rectangle(Point(0, 390), Point(400, 400))
    d = Rectangle(Point(390, 0), Point(400,400))

    borderA = a.draw()
    borderB = b.draw()
    borderC = c.draw()
    borderD = d.draw()

    print "borders calculated"

    #robo = Rectangle(Point(300, 300), Point(323, 371))
    
    robo = Circle(Point(320, 320), 10)

    setD = robo.draw()
    print "robot calculated. object has %d points" % len(setD)

    print robo.center

    points = []

    points.extend((minowski(setA, setD, robo.center)))
    print "minowski sum calculated.... %d points" % (len(points))
	
    points.extend((minowski(setB, setD, robo.center)))
    print "minowski sum calculated.... %d points" % (len(points))

    points.extend((minowski(setC, setD, robo.center)))
    print "minowski sum calculated.... %d points" % (len(points))

    # calculate border...
    points.extend((minowski(borderA, setD, robo.center)))
    points.extend((minowski(borderB, setD, robo.center)))
    points.extend((minowski(borderC, setD, robo.center)))
    points.extend((minowski(borderD, setD, robo.center)))

    print "borders calculated..."

    drawPoints(points, COLORC) # draw minkowski sum

    drawPoints(borderA, COLORA) # draw border
    drawPoints(borderB, COLORA) # draw border
    drawPoints(borderC, COLORA) # draw border
    drawPoints(borderD, COLORA) # draw border

    drawPoints(setA, COLORA) # draw object
    drawPoints(setB, COLORA) # draw object
    drawPoints(setC, COLORA) # draw object
    

    drawPoints(setD, COLORB) # draw robot...





if __name__ == "__main__":
    #check parameters
    if len(sys.argv) != 1:
        print "minowski.py"
        sys.exit(-1)

    # create main window
    mw = Tk()

    # create and position canvas and buttons
    cFr = Frame(mw, width=WIDTH, height=HEIGHT, relief="sunken", bd=1)
    cFr.pack(side="top")
    can = Canvas(cFr, width=WIDTH, height=HEIGHT)
    can.pack()
    cFr = Frame(mw)
    cFr.pack(side="left")
    eFr = Frame(mw)
    eFr.pack(side="right")
    bExit = Button(eFr, text="Quit", command=(lambda root=mw: quit(root)))
    bExit.pack()

    draw()
    mw.mainloop()