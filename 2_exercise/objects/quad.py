import os
import math
parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.sys.path.insert(0,parentdir) 
from objects.rectangle import Rectangle
from geometry import Point
from geometry import Vector

class Quad(Rectangle):

    def __init__(self, center, size):

        width = size / 2

        p1 = Point(center.a - width, center.b - width)
        p2 = Point(center.a + width, center.b + width)

        Rectangle.__init__(self, p1, p2)
