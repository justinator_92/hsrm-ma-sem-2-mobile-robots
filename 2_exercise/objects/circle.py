import os
import math
parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.sys.path.insert(0,parentdir) 
from geometry import Point
from geometry import Vector


class Circle:

    def __init__(self, center, radius):
        self.center = center
        self.radius = radius

    def draw(self):
        points = []

        for width in range(0, 400):
            for height in range(0, 400):
                p = Point(width, height)
                if((self.center - p).length() < self.radius):
                    points.append(p)

        return points