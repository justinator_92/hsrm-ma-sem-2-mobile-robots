from Tkinter import *
from Canvas import *
from pathfinder import Space

import tkFileDialog
import sys

WIDTH = 1000  # width of canvas
HEIGHT = 600  # height of canvas

TITLE = "D* Simulation"


class Gui:

    def __init__(self):
        # create main window
        global root
        root = Tk()
        root.title(TITLE)
        root.resizable(False, False)

        # create and position canvas and buttons
        cFr = Frame(root, width=WIDTH, height=HEIGHT, relief="sunken", bd=1)
        cFr.pack(side="top")

        # define options for opening or saving a file
        self.file_opt = options = {}

        can = Canvas(cFr, width=WIDTH, height=HEIGHT, bg="#ffffff")
        can.pack()

        img = PhotoImage(width=WIDTH, height=HEIGHT)
        can.create_image((WIDTH/2, HEIGHT/2), image=img, state="normal")

        cFr = Frame(root)
        cFr.pack(side="left")
        eFr = Frame(root)
        eFr.pack(side="right")

        # simulation button frame

        #but_frame = Frame(root)

        #play_image = PhotoImage(file="../resources/play.png", width=50, height=50).zoom(2, 2)

        #Button(but_frame, compound=TOP, width=50, height=50, image=play_image, bg='white').pack(side=LEFT)  # play
        #Button(but_frame, compound=TOP, width=50, height=50, image=play_image, bg='white').pack(side=BOTTOM)  # stop

        #but_frame.pack(side=BOTTOM)

        self.create_menu()

        self.space = Space(can, img, WIDTH, HEIGHT)  # init controller object...

        root.mainloop()

    def create_menu(self):
        """
        Create Menubar
        :return:
        """

        menubar = Menu(root, tearoff=0)
        menubar.add_command(label="Undo", command=self.click)
        menubar.add_command(label="Redo", command=self.click)

        menubar = Menu(root)

        # create a pulldown menu, and add it to the menu bar
        filemenu = Menu(menubar, tearoff=0)
        filemenu.add_command(label="Open Configuration", command=self.askopenfile)
        filemenu.add_command(label="Save Configuration", command=self.asksaveasfilename)
        filemenu.add_separator()
        filemenu.add_command(label="Exit", command=root.quit)
        menubar.add_cascade(label="File", menu=filemenu)

        # create more pulldown menus
        editmenu = Menu(menubar, tearoff=0)
        editmenu.add_command(label="New Goal", command=self.click)
        editmenu.add_command(label="New Fixpoints", command=self.reload_fixpoints)
        editmenu.add_command(label="Set Robot", command=self.click)
        editmenu.add_command(label="Draw Obstacle", command=self.click)
        menubar.add_cascade(label="Edit", menu=editmenu)

        settingsmenu = Menu(menubar, tearoff=0)
        settingsmenu.add_command(label="Open Configuration", command=self.askopenfile)
        menubar.add_cascade(label="Settings", menu=settingsmenu)

        helpmenu = Menu(menubar, tearoff=0)
        helpmenu.add_command(label="About", command=self.click)
        menubar.add_cascade(label="Help", menu=helpmenu)

        # display the menu
        root.config(menu=menubar)

    def askopenfile(self):
        """Returns an opened file in read mode."""
        # get filename
        filename = tkFileDialog.askopenfilename(**self.file_opt)
        print filename
        if filename is None or filename == "":
            self.space.load_configuration(filename)

    def reload_fixpoints(self):
        """ calculate new fixpoints """
        self.space.generate_points()

    def asksaveasfilename(self):
        """
        Returns an opened file in write mode.
        This time the dialog just returns a filename and the file is opened by your own code.
        """
        filename = tkFileDialog.asksaveasfilename(**self.file_opt)
        self.space.save_configuration(filename)

    def click(self):
        print "click"

    def quit(self, root=None):
        """ quit programm """
        if not root:
            sys.exit(0)
        root._root().quit()
        root._root().destroy()


if __name__ == "__main__":
    a = Gui()
