import Node


def astar(graph, start, goal):

    # initialization: calculating h values:
    for node in graph.keys():
        node.h = (node.point - goal.point).length()
        node.predecessor = None

    closedList = []
    openList = [start]

    while len(openList) > 0:

        # find node with lowest f
        current = min(openList, key=lambda x: x.f)

        if current == goal:
            return current

        openList.remove(current)
        closedList.append(current)

        # check for each successor
        expand_node(current, closedList, openList, graph)

    return False  # return error, no route was found..


def expand_node(current, closed_list, open_list, graph):
    for successor in graph[current]:

        if successor in closed_list:
            continue  # node was already evaluated...

        tentative_g = graph[current][successor]

        if successor in open_list and tentative_g >= successor.g:  # new way is not better than old one..
            continue

        successor.predecessor = current
        successor.g = tentative_g

        f = tentative_g + successor.h
        successor.f = f

        if successor not in open_list:
            open_list.append(successor)

if __name__ == '__main__':

    A = Node(0, 50, 0)
    B = Node("B", 50, 100)
    C = Node("C", 50, 0)
    D = Node("D", 100, 50)

    nodes = {A: {B: 1, C: 2},
             B: {A: 1, D: 5},
             C: {A: 2, D: 3},
             D: {B: 5, C: 3}}

    print astar(nodes, A, D)
