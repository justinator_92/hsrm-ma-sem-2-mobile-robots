from geometry import Point

import numpy as np

RESOLUTION = 20.


def create_middle(a=1):
    """ create start matrix """
    return np.matrix([[0, 2, 0, 0],
                      [-a, 0, a, 0],
                      [2 * a, -6 + a, 6 - 2 * a, -a],
                      [-a, 4 - a, -4 + a, a]])


def create_start():
    """ create matrix for middle points """
    return np.matrix([[0, 2, 0, 0],
                      [2, 0, 0, 0],
                      [-4, -5, 6, -1],
                      [2, 3, -4, 1]])


def create_end():
    """ create matrix for last point """
    return np.matrix([[0, 2, 0, 0],
                      [-1, 0, 1, 0],
                      [2, -6, 4, -2],
                      [-1, 4, -3, 2]])


class CRSpline(object):

    def __init__(self, startP, endP):
        """ constructor for catmull rom splines curve """
        self.start = create_start()
        self.middle = create_middle()
        self.end = create_end()

        self.startP = startP
        self.endP = endP

    def calculate_first_segment(self, p0, p1, p2):

        point_list = []
        a = 0.5

        for e in range(0, int(RESOLUTION)):

            t = e / RESOLUTION
            b = np.array([[1, t, t*t, t*t*t]])
            c = self.start
            d = np.array([[1, self.startP], p0, p1, p2])
            p = a * b.dot(c.dot(d))

            point_list.append(p)

        return point_list

    def calculate_middle_segment(self, p0, p1, p2, p3):

        point_list = []
        a = 0.5

        for e in range(0, int(RESOLUTION)):

            t = e / RESOLUTION
            b = np.array([[1, t, t*t, t*t*t]])
            c = self.middle
            d = np.array([p0, p1, p2, p3])
            p = a * b.dot(c.dot(d))

            point_list.append(p)

        return point_list

    def calculate_end_segment(self, p0, p1, p2):
        """ calculate end segment of spline """

        point_list = []

        for e in range(0, int(RESOLUTION)):

            t = e / RESOLUTION
            b = np.array([[1, t, t*t, t*t*t]])

            d = np.array([p0, p1, p2, [1, self.endP]])
            a = create_end().dot(d)
            temp = b.dot(a)

            p = 0.5 * temp
            point_list.append(p)

        return point_list

    def create_spline(self, point_list):
        """ create spline curve """

        ret = []

        for i in range(0, len(point_list)-1):

            if i == 0:  # start point
                ret.extend(self.calculate_first_segment(point_list[i], point_list[i+1], point_list[i+2]))
            elif i == len(point_list)-2:  # end points
                ret.extend(self.calculate_end_segment(point_list[i-1], point_list[i], point_list[i+1]))
            else:  # regular middle point
                p0, p1, p2, p3 = point_list[i-1], point_list[i], point_list[i+1], point_list[i+2]
                ret.extend(self.calculate_middle_segment(p0, p1, p2, p3))

        return [Point(point.item((0, 0)), point.item((0, 1))) for point in ret]


if __name__ == '__main__':

    points = [[20, 5], [22, 12], [34, 17], [34, 24], [22, 36], [22, 25], [18, 20], [12, 20], [17, 17], [16, 10]]

    points = [np.array(p) for p in points]

    spline = CRSpline(0, 0)
    print spline.create_spline(points)
