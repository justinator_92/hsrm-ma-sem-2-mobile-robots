from geometry import Point
from objects import Circle
from objects import Rectangle
from utils import Serializer
from pathfinder import PointGrid, Astar, Node, CRSpline


BLUE = "#0080FF"  # light blue
GREEN = "#FF0000"  # green
LIGHTBLUE = "#80C2FF"  # even lighter blue
BLACK = "#000000"  # black
RED = "#FF0000"  # red

SIZE = 10

FRAME = 4  # size of frame


class Space:

    def __init__(self, canvas, img, width, height):
        """
        Constructor for Space Class. This is class the mastermind that manages
        the entire application.
        :param canvas: the canvas to draw
        :param img: image to place pixels
        :param width: width of the GUI
        :param height: height of the GUI
        """

        self.endpoint = Node(Point(900, 500))  # TODO need to change this later!!!
        self.startpoint = Node(Point(100, 350))  # TODO need to change this later!!!

        self.serializer = Serializer()  # serializer to save and load configuration model
        self.grid = PointGrid.PointGrid(canvas, width, height, self.startpoint, self.endpoint)  # tool to create walkable nodes on the map
        self.canvas = canvas  # the canvas to draw all objects
        self.img = img  # image to set pixel manually on GUI

        self.width = width  # width of entire canvas
        self.height = height  # height of entire canvas

        # create robot...
        robot = Rectangle(Point(300, 300), Point(307, 307), RED)

        # create obstacles...
        o1 = Circle(Point(500, 300), 53, BLUE)
        o2 = Rectangle(Point(50, 50), Point(101, 151), BLUE)
        #o3 = Rectangle(Point(600, 100), Point(650, 400), BLUE)
        o4 = Rectangle(Point(50, 451), Point(150, 501), BLUE)

        # # create borders of environment...
        #b1 = Rectangle(Point(0, 0), Point(self.width, FRAME), BLUE)
        #b2 = Rectangle(Point(0, 0), Point(FRAME, self.height), BLUE)
        #b3 = Rectangle(Point(0, self.height - FRAME), Point(self.width, self.height), BLUE)
        #b4 = Rectangle(Point(self.width - FRAME, 0), Point(self.width, self.height), BLUE)

        self.objects = [o1, o2, o4]  # , o2, o3]  # o2, o3, o4, b1, b2, b3, b4]

        # calculate minkowski sum for all obstacles...
        for obj in self.objects:
            obj.minkowski(robot)

        self.grid.create_graph(self.objects)
        self.drawObjects(self.objects)

        #self.load_configuration("/home/justin/Schreibtisch/robotic/project/src/Test.conf")

        self.path = set()
        self.find_route()

    def find_route(self):

        end = Astar.astar(self.grid.graph, self.startpoint, self.endpoint)

        if not end:
            print "no path found..."
            return

        print end
        path = [end]

        points = [end.point]

        # draw path:
        while end.predecessor:
            path.append(end.predecessor)
            points.append(end.predecessor.point)
            end = end.predecessor

        for start, end in zip(path, path[1:]):
            self.path.add(self.canvas.create_line(start.point.x, start.point.y, end.point.x, end.point.y,
                                                  fill="#ff0000"))

        # draw spline curve...
        spline = CRSpline.CRSpline(0, 10)  # derivate 0, 0
        cat = spline.create_spline([[node.x, node.y] for node in points])
        print cat

        for p in cat:
            self.canvas.create_oval(p.x - 1, p.y - 1, p.x + 1, p.y + 1, fill="#000000", outline="#000000")


    def load_configuration(self, filename):
        """
        Open a configuration model
        :param filename:
        :return:
        """

        obj = self.serializer.deserialize(filename)
        print obj

        if obj:
            self.objects = obj
            self.drawObjects(self.objects)

    def save_configuration(self, filename):
        """
        saves all objects to file
        :param filename: filename
        """
        self.serializer.serialize(self.objects, filename)

    def drawPoints(self, points, color):
        """ draw (control-)points """
        for p in points:
            self.drawPoint(p, color)

    def drawPoint(self, p, color):
        """ draw a point in canvas """
        self.img.put(color, (p.a, p.b))

    def drawObject(self, obj):
        """
        draws a object
        :param obj: object to draw
        :return: None
        """
        for point in obj.points:
            self.drawPoint(point, obj.color)

    def drawObjects(self, objs):
        """
        draws a list of objects
        :param objs: objects to draw
        :return: None
        """
        for obj in objs:
            print obj.draw(self.canvas)
