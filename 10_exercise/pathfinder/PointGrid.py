from geometry import Point, Line
from scipy.spatial import Delaunay
from pathfinder import Node

import random
import numpy as np

SIZE = 2


class PointGrid(object):

    def __init__(self, canvas, width, height, startpoint, endpoint):
        """
        Constructor for Point Grid that places stastistical points
        onto the map
        :param canvas: the canvas to draw the points and lines
        :param objects: all objects that are in scene
        """
        self.canvas = canvas  # canvas to draw objects
        self.width = width  # width of canvas
        self.height = height  # height of canvas
        # self.objects = objects

        self.endpoint = endpoint  # TODO: need to change this later...
        self.startpoint = startpoint  # TODO: need to change this later...

        self.grid_points = set()
        self.lines = set()

        self.number_of_points = 100

        self.graph = {}

    def generate_points(self, objects):
        """
        Generate statistical points on grid
        :param objects: current objects of scene
        :return:
        """
        points = []

        while len(points) < self.number_of_points:
            valid = True
            p = Point(random.randint(5, self.width), random.randint(5, self.height))
            for obj in objects:
                if obj.point_in_polygon(p):
                    valid = False
                    break

            if valid:
                #        for p in [Point(383, 84), Point(288, 186), Point(405, 540), Point(879, 488), Point(401,170)]:
                node = Node(p)
                print "add node", node
                points.append(node)
                object_id = self.canvas.create_oval(p.x - SIZE, p.y - SIZE, p.x + SIZE, p.y + SIZE, tags="event",
                                                    fill="#ff0000", outline="#ff0000")

                self.grid_points.add(object_id)

        points.append(self.endpoint)
        self.canvas.create_oval(self.endpoint.point.x - 50, self.endpoint.point.y - 50, self.endpoint.point.x + 50,
                                self.endpoint.point.y + 50,
                                fill='#ffff00')

        points.append(self.startpoint)
        self.canvas.create_oval(self.startpoint.point.x - 50, self.startpoint.point.y - 50, self.startpoint.point.x + 50,
                                self.startpoint.point.y + 50,
                                fill='#ff00ff')

        return points

    def create_graph(self, objects):
        """
        create points on grid
        :return:
        """
        for line in self.lines:
            self.canvas.delete(line)

        for fix in self.grid_points:
            self.canvas.tag_unbind(fix, "<ButtonPress-1>")
            self.canvas.delete(fix)

        self.lines.clear()
        self.grid_points.clear()

        points = self.generate_points(objects)  # generate points on grid

        # TODO: replace with own triangulation...
        pp = np.array([[node.point.x, node.point.y] for node in points])
        tri = Delaunay(pp)

        self.graph.clear()

        for e in tri.simplices:
            node1 = points[e[0]]
            node2 = points[e[1]]
            node3 = points[e[2]]

            nodes = {Line(node1.point, node2.point): (node1, node2),
                     Line(node2.point, node3.point): (node2, node3),
                     Line(node3.point, node1.point): (node3, node1)}

            for line, values in nodes.iteritems():
                valid = True

                for obj in objects:
                    if obj.intersect_with_line(line):
                        valid = False
                        break

                if valid:  # connection between two points is valid. add node to graph..

                    distance = (values[0].point - values[1].point).length()  # calculate vector between points..

                    if not values[0] in self.graph:
                        self.graph[values[0]] = {values[1]: distance}
                    else:
                        self.graph[values[0]][values[1]] = distance

                    if not values[1] in self.graph:
                        self.graph[values[1]] = {values[0]: distance}
                    else:
                        self.graph[values[1]][values[0]] = distance

                    self.lines.add(self.canvas.create_line(line.p1.x, line.p1.y, line.p2.x, line.p2.y, fill="#cccccc"))
