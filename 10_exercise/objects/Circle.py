from geometry import Point
from objects.Primitive import Primitive

import os

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.sys.path.insert(0, parentdir)


class Circle(Primitive):

    def __init__(self, center, radius, color):
        """ constructor for circle """
        Primitive.__init__(self)

        self.center = center
        self.radius = radius

        self.color = color

        self.points = []  # list that includes all points...
        self.calculatePoints()  # calculate own points...

        print self

    def intersect(self, p):
        """ test if point intersects with circle """

        if (self.center - p).length() < self.radius:
            self.points.append(p)
            return True

        return False

    def calculatePoints(self):
        """
        calulates Points
        :return:
        """

        for x in range(int(self.center.x - self.radius), int(self.center.x + self.radius)):
            for y in range(int(self.center.y - self.radius), int(self.center.y + self.radius)):
                p = Point(x, y)

                if self.intersect(p):
                    self.points.append(p)

    def draw(self, canvas):
        """
        draw a circle on a canvas
        :param canvas:
        :return:
        """

        self.draw_minkowski(canvas)
        return canvas.create_oval(self.center.x - self.radius, self.center.y - self.radius,
                                  self.center.x + self.radius, self.center.y + self.radius,
                                  fill=self.color, outline=self.color)

    def __repr__(self):
        """
        string representation
        :return:
        """
        return "Circle [Center: %s, Radius: %d, Points: %d]" % (self.center, self.radius, len(self.points))

