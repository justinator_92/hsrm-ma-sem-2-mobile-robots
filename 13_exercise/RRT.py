from Tkinter import *
from Canvas import *
from pathfinder import Space
from geometry import Point


import sys

WIDTH = 400  # width of canvas
HEIGHT = 400  # height of canvas

TITLE = "Rapid-exploring Random Tree"


class Gui:

    def __init__(self):
        # create main window
        global root
        root = Tk()
        root.title(TITLE)
        root.resizable(False, False)

        # create and position canvas and buttons
        cFr = Frame(root, width=WIDTH, height=HEIGHT, relief="sunken", bd=1)
        cFr.pack(side="top")

        # define options for opening or saving a file
        self.file_opt = options = {}

        can = Canvas(cFr, width=WIDTH, height=HEIGHT, bg="#ffffff")
        can.bind("<ButtonPress-1>", self.goal)
        can.pack()

        img = PhotoImage(width=WIDTH, height=HEIGHT)
        can.create_image((WIDTH/2, HEIGHT/2), image=img, state="normal")

        cFr = Frame(root)
        cFr.pack(side="left")
        eFr = Frame(root)
        eFr.pack(side="right")

        self.space = Space(can, img, WIDTH, HEIGHT)  # init controller object...

        root.mainloop()

    def goal(self, event):
        self.space.clear_canvas()
        path = self.space.RRT(Point(event.x, event.y))
        if path:
            self.space.draw_spline(path)

    def click(self):
        print "click"

    def quit(self, root=None):
        """ quit programm """
        if not root:
            sys.exit(0)
        root._root().quit()
        root._root().destroy()


if __name__ == "__main__":
    a = Gui()
