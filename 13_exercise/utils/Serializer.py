import pickle
import os


class Serializer(object):

    def __init__(self):
        pass


    def serialize(self, obj, file_name):
        """
        Serialize a given object using pickle
        :param obj: object to serialize
        :param file_name: fileName to save object
        """

        file_object = open(file_name, 'wb')
        pickle.dump(obj, file_object)
        file_object.close()


    def deserialize(self, file_name):
        """
        Deserialize an object using pickle
        :param file_name: filename pointing to file
        :return: deserialized object or None
        """

        path = os.path.join(os.getcwd(), file_name)
        print path

        file_object = open(path, 'r')
        b = pickle.load(file_object)

        return b


    def get_list_objects(self, file_name):

        try:
            points = self.deserialize(file_name)
            return points

        except:
            print "error while reading files"

        return []
