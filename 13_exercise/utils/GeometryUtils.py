from geometry import Point


TURN_LEFT, TURN_RIGHT, TURN_NONE = (1, -1, 0)

# def turn(p, q, r):
#    return cmp((q[0] - p[0])*(r[1] - p[1]) - (r[0] - p[0])*(q[1] - p[1]), 0)


def turn(p, q, r):
    return cmp((q.x - p.x)*(r.y - p.y) - (r.x - p.x)*(q.y - p.y), 0)


def _keep_left(hull, r):
    while len(hull) > 1 and turn(hull[-2], hull[-1], r) != TURN_LEFT:
        hull.pop()
    if not len(hull) or hull[-1] != r:
        hull.append(r)
    return hull


def convex_hull(points):
    points = sorted(points)
    l = reduce(_keep_left, points, [])
    u = reduce(_keep_left, reversed(points), [])
    result = l.extend(u[i] for i in xrange(1, len(u)-1)) or l
    return [result[0]] + result[::-1]  # make result list clockwise


def barycentric_combination(points):

    d = 1. / len(points)
    x_comp = sum(map(lambda p: p.x, points))
    y_comp = sum(map(lambda p: p.y, points))

    return Point(x_comp * d, y_comp * d)


def point_in_polygon(polygon, point):

    for (p, q) in zip(polygon, polygon[1:]):
        if (q-p).normal().dot(point - p) > 0:
            return False
    return True


def point_between_points(point_start, point_end, point_test):

    # swap points
    if point_end.x < point_start.x:
        point_start, point_end = point_end, point_start

    epsilon = 200

    # check if points are aligned...
    x = (point_end - point_start)
    y = (point_test - point_start)

    disc = abs(x.cross(y))

    if disc > epsilon:  # comparison for float values, != 0 for integer values...
        return False

    dot_product = x.dot(y)
    if dot_product < 0:
        return False

    length = (point_end - point_start).length()**2
    if dot_product > length:
        return False

    return True

if __name__ == '__main__':
    a = Point(372, 225)
    b = Point(766, 230)
    c = Point(90, 221.42132)

    normal = (b-a).normalized()


    p1 = Point(10, 70)
    p2 = Point(20, 90)
    p3 = Point(40, 130)

    x = Point(30, 100)
    y = Point(40, 100)
    z = Point(60, 100)

    print point_between_points(b, c, a)
    #print point_between_points(p1, p3, p2)
    #print point_between_points(x, z, y)


