from geometry import Point, Line
from utils import GeometryUtils as g


import math


class Primitive(object):
    """
    Base class for Primitive objects...
    """

    def __init__(self):
        """ constructor for Primitive geometric object """

        self.points = []
        self.min = []
        self.boundingBox = None

    def minkowski(self, obj):
        """ calculates Minkowski sum """

        points = set()
        progress, count = len(self.points) / 10, 0

        for pointA in self.points:
            if count > progress:
                print "yes"
                count = 0
            else:
                count += 1
            for pointB in obj.points:
                vector = pointB - obj.center
                p = pointA + vector
                if p.x >= 0 and p.y >= 0:
                    points.add(p)

        self.min = g.convex_hull(list(points))
        print "minkowski sum", self.min

        #print "minimum: ", min(self.min)
        # calculating bounding sphere...
        #upperleft = Point(min([p.x for p in self.min]), min([p.y for p in self.min]))
        #lowerright = Point(max([p.x for p in self.min]), max([p.y for p in self.min]))
        #print upperleft, lowerright
        #print self.center

#        l1 = (self.center - upperleft).length()
#        l2 = (self.center - lowerright).length()

#        self.boundingBox = BoudingSphere.BoundingSphere(self.center, max(l1, l2))

    def rotate_point(self, point, angle):
        """ rotate a point around origin with given angle """
        translate = Point(0, 0) - self.center  # create translate vector to origin
        t = point + translate  # translate point to origin
        rotated = Point(t.x * math.cos(angle) - t.y * math.sin(angle), t.x * math.sin(angle) + t.y * math.cos(angle))
        return rotated - translate

    def point_intersection(self, point):
        return g.point_in_polygon(self.min, point)

    def calculate_intersection(self, other):

        # check if start- and endpoint of line is in polygon...
        if g.point_in_polygon(self.min, other.p1) or g.point_in_polygon(self.min, other.p2):
            print "point is in polygon... TRUE!!!"
            return True

        # make lines from points in minkowski sum:
        for (p1, p2) in zip(self.min, self.min[1:]):
            line = Line(p1, p2)
            p = line.intersect_lines(other)
            #print p
            if p and g.point_between_points(other.p1, other.p2, p) and g.point_between_points(p1, p2, p):
                return True

        return False

    def draw_minkowski(self, canvas):
        """ draw minkowski sum to canvas """
        if len(self.min) > 0:
            return canvas.create_polygon([[p.x, p.y] for p in self.min], fill="#80C2FF", outline=None)

