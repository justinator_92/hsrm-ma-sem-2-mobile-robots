from objects.Rectangle import Rectangle
from geometry import Point

import os

parent_directory = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.sys.path.insert(0, parent_directory)


class Quad(Rectangle):

    def __init__(self, center, size, color):

        width = size / 2

        p1 = Point(center.a - width, center.b - width)
        p2 = Point(center.a + width, center.b + width)

        Rectangle.__init__(self, p1, p2, color)
