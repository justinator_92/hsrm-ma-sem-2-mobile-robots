from objects.Primitive import Primitive
from geometry import Point
from utils import barycentric_combination

import os

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.sys.path.insert(0, parentdir)


class Triangle(Primitive):

    def __init__(self, a, b, c, color):
        """
        constructor for Triangle
        :param a:
        :param b:
        :param c:
        """

        Primitive.__init__(self)

        self.a = a
        self.b = b
        self.c = c

        self.center = barycentric_combination([self.a, self.b, self.c])
        self.color = color

    def draw(self, canvas):
        """
        draw shape on canvas
        :param canvas:
        :return:
        """

        points = [self.a.x, self.a.y, self.b.x, self.b.y, self.c.x, self.c.y]

        #        if(len(self.min) > 0):
        #            canvas.create_polygon([item for sublist in self.min for item in sublist], fill="#ff00aa")
        self.draw_minkowski(canvas)
        return canvas.create_polygon(points, fill=self.color)

    def calculate_points(self):
        """
        Calculate points
        :return:
        """

        polygon = [self.a, self.b, self.c, self.a]

        for x in range(self.p0.x, self.p2.x + 1):
            for y in range(self.p0.y, self.p2.y + 1):

                p = Point(x, y)
                if self.intersect(p, polygon):
                    self.points.append(p)

    def rotate(self, angle):
        """
        rotate the rectangle around angle
        :param angle: angle in radians
        """

        self.a = self.rotate_point(self.a, angle)
        self.b = self.rotate_point(self.b, angle)
        self.c = self.rotate_point(self.c, angle)

        #print self.a, self.b, self.c

        return [self.a.x, self.a.y, self.b.x, self.b.y, self.c.x, self.c.y]

    def __repr__(self):
        return "Triangle: [a: %s, b: %s, c: %s, center: %s]" % (self.a, self.b, self.c, self.center)
