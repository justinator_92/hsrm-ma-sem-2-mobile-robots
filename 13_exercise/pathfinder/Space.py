from geometry import Point
from objects import Circle
from objects import Rectangle
from pathfinder import CRSpline
from utils import Serializer

import time
import random

BLUE = "#0080FF"  # light blue
GREEN = "#FF0000"  # green
LIGHTBLUE = "#80C2FF"  # even lighter blue
BLACK = "#000000"  # black
RED = "#FF0000"  # red

SIZE = 10
POINT = 0.2

FRAME = 4  # size of frame


class Space:

    def __init__(self, canvas, img, width, height):
        """
        Constructor for Space Class. This is class the mastermind that manages
        the entire application.
        :param canvas: the canvas to draw
        :param img: image to place pixels
        :param width: width of the GUI
        :param height: height of the GUI
        """

        self.obj_indices = []

        self.canvas = canvas  # the canvas to draw all objects
        self.img = img  # image to set pixel manually on GUI

        self.width = width  # width of entire canvas
        self.height = height  # height of entire canvas

        self.serializer = Serializer()

        # create robot...
        robot = Rectangle(Point(300, 300), Point(310, 310), RED)

        # create obstacles...
        #o1 = Circle(Point(250, 100), 40, BLUE)
        #o2 = Rectangle(Point(80, 100), Point(120, 300), BLUE)
        #o3 = Rectangle(Point(200, 200), Point(300, 300), BLUE)

        #self.objects = [o1, o2, o3]  # , o2, o3]  # o2, o3, o4, b1, b2, b3, b4]
        self.objects = self.serializer.get_list_objects("configuration.conf")
        # calculate minkowski sum for all obstacles...
        #for obj in self.objects:
        #    obj.minkowski(robot)

        self.draw_objects(self.objects)
        self.serializer.serialize(self.objects, "configuration.conf")
        self.start = Point(self.width / 2 - 20, self.height / 2)
        self.draw_point(self.start, "green")


    def clear_canvas(self):

        while len(self.obj_indices) > 0:
            self.canvas.delete(self.obj_indices.pop())

    def RRT(self, goal, delta=20, teta=20, n=500):

        self.obj_indices.append(self.draw_point(goal, "blue"))

        tree = [self.start]

        for _ in range(0, n):

            valid = True  # flag if p_rand was place on a valid position...

            # 1. generate random point in configuration model
            p_rand = Point(random.randint(0, self.width), random.randint(0, self.height))

            # 2. Search for Node p_rrt with shortest distance to p_rand
            (p_rrt, dist) = self.find_minimum(tree, p_rand)

            # 3. combine p_rand and p_rrt and define p_new with a distance delta from p_rrt
            # if distance p_rand p_rrt < delta, set p_new = p_rand
            p_new = p_rrt + delta * (p_rand - p_rrt).normalized()

            if (p_rand - p_rrt).length() < delta:
                p_new = p_rand

            # 4. check if there is a collision between p_new and p_rrt
            # if yes then

            for obj in self.objects:
                if obj.point_intersection(p_new):
                    valid = False
                    break

            if not valid:
                continue

            self.obj_indices.append(self.draw_point(p_new, "#6a6a6a", 2))
            tree.append(p_new)
            if True:
                p_rrt.append_node(p_new)

            self.obj_indices.append(self.canvas.create_line(p_new.x, p_new.y, p_rrt.x, p_rrt.y, fill="#a3a3a3"))
            time.sleep(.05)

            # 5. check if goal is reachable under length of teta
            if (goal-p_new).length() < teta:
                print "goal found"
                self.obj_indices.append(self.canvas.create_line(p_new.x, p_new.y, goal.x, goal.y, fill="#a3a3a3"))

                point_list = [goal, p_new]
                print p_rrt
                while p_new.parent:  # find path from goal to start...
                    p_new = p_new.parent
                    point_list.append(p_new)
                    print p_new
                return point_list

        return None

    def find_minimum(self, tree, p_rand):

        node = tree[0]
        min_dist = float('inf')

        for n in tree:
            l = (n - p_rand).length()
            if l < min_dist:
                min_dist, node = l, n

        return node, min_dist

    def draw_spline(self, control_polygon):

        # draw spline curve...
        spline = CRSpline.CRSpline(0, 10)  # derivate 0, 0
        cat = spline.create_spline([[node.x, node.y] for node in control_polygon])
        print cat

        for p in cat:
            self.obj_indices.append(self.canvas.create_oval(p.x - POINT, p.y - POINT, p.x + POINT, p.y + POINT,
                                    fill="#000000", outline="#000000"))

    def draw_point(self, p, color, radius=3):
        """ draw a point in canvas """

        idx = self.canvas.create_oval(p.x - radius, p.y - radius, p.x + radius, p.y + radius,
                                                        fill=color, outline=color)
        self.canvas.update()
        return idx

    def draw_object(self, obj):
        """
        draws a object
        :param obj: object to draw
        :return: None
        """
        for point in obj.points:
            self.drawPoint(point, obj.color)

    def draw_objects(self, objs):
        """
        draws a list of objects
        :param objs: objects to draw
        :return: None
        """
        for obj in objs:
            print obj.draw(self.canvas)
