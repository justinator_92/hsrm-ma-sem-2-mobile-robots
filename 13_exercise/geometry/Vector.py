import math

class Vector(object):
    ''' Vektorklasse fuer Vektoren im R2 '''
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def normalized(self):
        ''' bringe Vektor auf Laenge 1 '''
        l = self.length()
        if l != 0:
            self.x /= l
            self.y /= l
        return Vector(self.x, self.y)

    def length(self):
        ''' berechne Norm des Vektors '''
        return math.sqrt(self.x ** 2 + self.y ** 2)

    def __repr__(self):
        ''' String-Repraesentation des Vektors '''
        return "Vector (%.9f, %.9f)\n" % (self.x, self.y)

    def __add__(self, v):
        """ komponentenweise addieren """
        return Vector(self.x + v.x, self.y + v.y)

    def __sub__(self, v):
        ''' komponentenweise subtrahieren '''
        return Vector(self.x - v.a, self.y - v.b)

    def __rsub__(self, v):
        ''' rechtsseiter Operator - wird an __sub__ delegiert '''
        return self.__sub__(v)

    def __div__(self, v):
        ''' dividiere komponentenweise durch skalar '''
        assert type(v) == int, "can't divide vector with" + v
        return Vector(self.x / v, self.y / v)

    def __mul__(self, v):
        ''' komponentenweise multiplizieren '''
        if type(v) == type(self):
            return Vector(self.x * v.a, self.y * v.b)
        if type(v) in [float, int]:  # skalares multiplizieren
            return Vector(self.x * v, self.y * v)
        return None

    def __rmul__(self, v):
        ''' rechtsseitiger Operator, delegiert Befehl an linksseitigen weiter '''
        return self.__mul__(v)

    def dot(self, v):
        ''' normales skalarprodukt mit Vektorinstanz und Operand '''
        return self.x * v.x + self.y * v.y

    def scale(self, number):
        ''' skaliert einen Vektor komponentenweise mit einem skalar '''
        return Vector(self.x * number, self.y * number)

    def normal(self):
        ''' errechnet normale mit 2D Drehtrick '''
        return Vector(-self.y, self.x)

