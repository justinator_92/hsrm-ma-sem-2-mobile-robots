from geometry import Vector

class Point(object):
    ''' Punktklasse mit allen benoetigten Operationen im R3'''
    

    def __init__(self, a, b):
        self.a = a
        self.b = b


    def __repr__(self):
        ''' String-Repreasentation '''
        return "Point (%f, %f)\n" % (self.a, self.b)

    def __sub__(self, p1):
        ''' subtrahiert komponentenweise '''
        if(type(p1) == type(self)):
        	return Vector(self.a - p1.a, self.b - p1.b)
        if(type(p1) == Vector):
        	return Point(self.a - p1.a, self.b - p1.b)
        return None

    def __add__(self, v):
        ''' addiert einen Vektor mit einem Punkt komponentenweise '''
        assert type(v) == Vector, "Addition muss Punkt + Vektor sein"
        return Point(self.a + v.a, self.b + v.b)

    def __eq__(self, other):
        return self.a == other.a and self.b == other.b

    def __hash__(self):
        return hash((self.a, self.b))


if __name__ == '__main__':
    a = set()
    p1 = Point(10, 10)
    p2 = Point(20, 20)
    p3 = Point(10, 10)
    p4 = Point(5, 5)
    p5 = Point(5, 5)

    a.add(p1)
    a.add(p2)
    a.add(p3)
    a.add(p4)
    a.add(p5)

    print len(a)