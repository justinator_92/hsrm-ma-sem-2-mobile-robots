from Tkinter import *
from Canvas import *
from geometry import Point
from geometry import Vector
from objects import Circle
from objects import Quad
from objects import Rectangle
import math
import sys

WIDTH  = 400 # width of canvas
HEIGHT = 400 # height of canvas

HPSIZE = 2 # half of point size (must be integer)
COLORA = "#0080FF" # light blue
COLORB = "#FF0000" # green
COLORC = "#80C2FF" # even lighter blue

pointList = []   # list of points


def drawPoints(points, color):
    """ draw (control-)points """
    for p in points:
        element = can.create_oval(p.a-HPSIZE, p.b-HPSIZE, p.a+HPSIZE, p.b+HPSIZE, fill=color, outline=color)

def minowski(setA, setB, center):
    """ calculates minowski sum """
    points = set()

    for pointA in setA:
        for pointB in setB:
            vecB = pointB - center
            v = pointA + vecB
            points.add(v)
            
    return points


def quit(root=None):
    """ quit programm """
    if root==None:
        sys.exit(0)
    root._root().quit()
    root._root().destroy()


def draw2():
    """ draw elements """
    obj1 = Quad(Point(250, 250), 71)
    obj1.rotate(-30)
    setA = obj1.draw()
    print "setA calculated. object has %d points" % (len(setA))

    obj2 = Circle(Point(350, 350), 20)
    setB = obj2.draw()
    print "setB calculated. object has %d points" % (len(setB))
    
    points = minowski(setA, setB, obj2.center)
    print "minowski sum calculated.... %d points" % (len(points))
    drawPoints(points, COLORA)
    drawPoints(setA, COLORB)
    drawPoints(setB, COLORC)



def draw():
    """ draw elements """
    obj1 = Quad(Point(250, 250), 71)
    obj1.rotate(-30)
    setA = obj1.draw()
    print "setA calculated. object has %d points" % (len(setA))

    obj2 = Circle(Point(350, 350), 20)
    setB = obj2.draw()
    print "setB calculated. object has %d points" % (len(setB))
    
    points = minowski(setA, setB, obj2.center)
    print "minowski sum calculated.... %d points" % (len(points))
    drawPoints(points, COLORA)
    drawPoints(setA, COLORB)
    drawPoints(setB, COLORC)


if __name__ == "__main__":
    #check parameters
    if len(sys.argv) != 2:
        print "minowski.py 1"
        sys.exit(-1)

    choice = int(sys.argv[1])

    if(choice not in [1, 2]):
        print "please enter 1 or 2 for exercise A or B"
        sys.exit(-1)

    # create main window
    mw = Tk()

    # create and position canvas and buttons
    cFr = Frame(mw, width=WIDTH, height=HEIGHT, relief="sunken", bd=1)
    cFr.pack(side="top")
    can = Canvas(cFr, width=WIDTH, height=HEIGHT)
    can.pack()
    cFr = Frame(mw)
    cFr.pack(side="left")
    eFr = Frame(mw)
    eFr.pack(side="right")
    bExit = Button(eFr, text="Quit", command=(lambda root=mw: quit(root)))
    bExit.pack()

    if choice == 1:
        draw()
    else:
        draw2()

    mw.mainloop()
    
