import sys

def shortestpath(graph,start,end,visited=[],distances={},predecessors={}):
    """Find the shortest path between start and end nodes in a graph"""
    # we've found our end node, now find the path to it, and return
    if start==end:
        path=[]
        while end != None:
            path.append(end)
            end=predecessors.get(end,None)
        return distances[start], path[::-1]
    # detect if it's the first time through, set current distance to zero
    if not visited: distances[start]=0
    # process neighbors as per algorithm, keep track of predecessors
    for neighbor in graph[start]:
        if neighbor not in visited:
            neighbordist = distances.get(neighbor,sys.maxint)
            tentativedist = distances[start] + graph[start][neighbor]
            if tentativedist < neighbordist:
                distances[neighbor] = tentativedist
                predecessors[neighbor]=start
    # neighbors processed, now mark the current node as visited
    visited.append(start)
    # finds the closest unvisited node to the start
    unvisiteds = dict((k, distances.get(k,sys.maxint)) for k in graph if k not in visited)
    closestnode = min(unvisiteds, key=unvisiteds.get)
    # now we can take the closest node and recurse, making it current
    return shortestpath(graph,closestnode,end,visited,distances,predecessors)

if __name__ == "__main__":
    graph = {'s': {'a': 30, 'd': 100, 'f': 80},
            'a': {'b': 40, 's': 30},
            'd': {'s': 100, 'e': 60,},
            'f': {'s': 80, 'e': 100, 'g': 80},
            'b': {'a': 40, 'c': 70, 'e': 20},
            'e': {'d': 60, 'b': 20, 'f': 100, 'h': 30, 'z' : 80},
            'g': {'f': 80, 'h': 90},
            'c': {'b': 70, 'z': 40},
            'z': {'e': 80, 'c': 40, 'h': 10},
            'h': {'e': 30, 'z': 10, 'g': 90}}
    print shortestpath(graph,'c','c')