from geometry import Point


class Line(object):
    """ this object represents a homogenious line """

    def __init__(self, p1, p2):
        """ constructor for line object"""

        self.l1 = p1.y * 1 - 1 * p2.y
        self.l2 = 1 * p2.x - p1.x * 1
        self.l3 = p1.x * p2.y - p1.y * p2.x

        self.p1 = p1
        self.p2 = p2

    def intersect_lines(self, other):
        a = self.l2 * other.l3 - self.l3 * other.l2
        b = self.l3 * other.l1 - self.l1 * other.l3
        c = self.l1 * other.l2 - self.l2 * other.l1

        if c == 0:
            return None

        return Point(a / c, b / c)  # clipping to 2D-coordinates

    def __repr__(self):
        return "Line: [%f, %f, %f]\n" % (self.l1, self.l2, self.l3)

if __name__ == '__main__':
    l1 = Line(Point(1, 1), Point(4, 4))
    l2 = Line(Point(1, 4), Point(4, 1))
    l3 = Line(Point(2, 2), Point(5, 5))

    print l1, l2, l3
    print l1.intersect_lines(l2)
    print l1.intersect_lines(l3)

