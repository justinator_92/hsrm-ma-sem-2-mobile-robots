import random

from geometry import Point, Line
from objects import Circle
from objects import Rectangle
from utils import Serializer
from scipy.spatial import Delaunay

import numpy as np

BLUE = "#0080FF"  # light blue
GREEN = "#FF0000"  # green
LIGHTBLUE = "#80C2FF"  # even lighter blue
BLACK = "#000000"  # black
RED = "#FF0000"  # red

HPSIZE = 2

FRAME = 4  # size of frame


class Space:

    def __init__(self, canvas, img, width, height):
        """ constructor for space object """

        self.fixpoints = set()
        self.serializer = Serializer()
        self.canvas = canvas
        self.img = img

        self.width = width
        self.height = height

        self.lines = set()

        robot = Rectangle(Point(300, 300), Point(306, 306), RED)

        # create obsticals...
        o1 = Circle(Point(500, 300), 51, BLUE)
        o2 = Rectangle(Point(50, 50), Point(100, 150), BLUE)
        #o3 = Rectangle(Point(600, 100), Point(650, 400), BLUE)
        o4 = Rectangle(Point(50, 450), Point(150, 500), BLUE)

        # # create borders of environment...
        #b1 = Rectangle(Point(0, 0), Point(self.width, FRAME), BLUE)
        #b2 = Rectangle(Point(0, 0), Point(FRAME, self.height), BLUE)
        #b3 = Rectangle(Point(0, self.height - FRAME), Point(self.width, self.height), BLUE)
        #b4 = Rectangle(Point(self.width - FRAME, 0), Point(self.width, self.height), BLUE)

        self.objects = [o1, o2, o4]  # , o2, o3]  # o2, o3, o4, b1, b2, b3, b4]

        # calculate minkowski sum...

        for obj in self.objects:
            obj.minkowski(robot)

        self.generatePoints()
        self.drawObjects(self.objects)

        #self.load_configuration("/home/justin/Schreibtisch/robotic/project/src/Test.conf")

    def generatePoints(self, amount=200):
        """
        generates random statistically fix points
        """

        for line in self.lines:
            self.canvas.delete(line)

        self.lines.clear()

        points = []

        while len(points) < amount:
            valid = True

            p = Point(random.randint(5, self.width), random.randint(5, self.height))
#            p = Point(random.randint(400, 600), random.randint(200, 400))
            for obj in self.objects:
                if obj.point_in_polygon(p):
                    valid = False
                    break

            if valid:
                points.append(p)
                self.lines.add(self.canvas.create_oval(p.x - HPSIZE, p.y - HPSIZE, p.x + HPSIZE, p.y + HPSIZE,
                                                       outline="#ff0000"))


        # TODO: replace with own triangulation...
        pp = np.array([[p.x, p.y] for p in points])
        tri = Delaunay(pp)

        for e in tri.simplices:
            p1 = Point(points[e[0]].x, points[e[0]].y)
            p2 = Point(points[e[1]].x, points[e[1]].y)
            p3 = Point(points[e[2]].x, points[e[2]].y)

            l1 = Line(p1, p2)
            l2 = Line(p2, p3)
            l3 = Line(p3, p1)

            for line in [l1, l2, l3]:

                valid = True

                for obj in self.objects:
                    if obj.intersect_with_line(line):
                        valid = False
                        break

                if valid:
                    self.lines.add(self.canvas.create_line(line.p1.x, line.p1.y, line.p2.x, line.p2.y, fill="#cccccc"))

#            self.lines.add(self.canvas.create_line(p2[0], p2[1], p3[0], p3[1], fill=BLACK))
#            self.lines.add(self.canvas.create_line(p3[0], p3[1], p1[0], p1[1], fill=BLACK))

    def load_configuration(self, filename):
        """
        Open a configuration model
        :param filename:
        :return:
        """

        obj = self.serializer.deserialize(filename)
        print obj

        if obj != None:
            self.objects = obj
            self.drawObjects(self.objects)

    def save_configuration(self, filename):
        """
        saves all objects to file
        :param filename: filename
        """
        self.serializer.serialize(self.objects, filename)

    def drawPoints(self, points, color):
        """ draw (control-)points """
        for p in points:
            self.drawPoint(p, color)

    def drawPoint(self, p, color):
        """ draw a point in canvas """
        # element = can.create_oval(p.a-HPSIZE, p.b-HPSIZE, p.a+HPSIZE, p.b+HPSIZE, fill=color, outline=color)
        self.img.put(color, (p.a, p.b))

    def drawObject(self, obj):
        """
        draws a object
        :param obj: object to draw
        :return: None
        """

        #for mi in obj.mink:
        #    self.drawPoint(mi, "#80C2FF")

        for point in obj.points:
            self.drawPoint(point, obj.color)

    def drawObjects(self, objs):
        """
        draws a list of objects
        :param objs: objects to draw
        :return: None
        """

        for obj in objs:
            print obj.draw(self.canvas)

#        for obj in objs:
#            for p in obj.mink:
#                self.drawPoint(p, "#80C2FF")
