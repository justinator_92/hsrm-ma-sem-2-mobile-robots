import Primitive

import os
import time

from objects.Primitive import Primitive

parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.sys.path.insert(0, parentdir)
from geometry import Point

class Triangle(Primitive):

    def __init__(self, a, b, c):
        """
        constructor for Triangle
        :param a:
        :param b:
        :param c:
        """

        self.a = a
        self.b = b
        self.c = c

        Primitive.__init__(self)

    def __repr__(self):
        return "Triangle: [A: %s, B: %s, C: %]" % (self.a, self.b, self.c)