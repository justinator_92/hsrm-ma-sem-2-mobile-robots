from geometry import Point, Vector

import math


class BoundingSphere(object):

    def __init__(self, center, radius):
        self.c = center
        self.r = radius

    def has_intersection(self, start, end):
        """
        check if line consists of points p1 and p2 have intersection with
        bounding sphere
        :param start: start point of line l1
        :param end: end point of line l1
        :return: flag if line (p1, p2) has intersection with current bounding sphere
        """

        d = end - start
        f = start - self.c

        a = d.dot(d)
        # print d, a
        b = 2 * f.dot(d)
        c = f.dot(f) - self.r*self.r

        discriminant = b * b - 4 * a * c
        # print discriminant
        if discriminant < 0:
            return False

        discriminant = math.sqrt(discriminant)

        t1 = (-b - discriminant) / (2. * a)
        t2 = (-b + discriminant) / (2. * a)

        if 0 <= t1 <= 1:
            return True

        if 0 <= t2 <= 1:
            return True

        return False

    def point_in_sphere(self, point):
        """
        Test if point is in bounding sphere
        :param point: given point to test
        :return: flag if point is in bouding sphere
        """
        if (self.c - point).length() < self.r:
            return True
        return False


if __name__ == '__main__':

    r = BoundingSphere(Point(7, 4), 2)
    p1 = Point(2, 6)
    p2 = Point(5, 1)

    print r.has_intersect(p1, p2)

    p1 = Point(5, 7)
    p2 = Point(9, 2)

    print r.has_intersect(p1, p2)
